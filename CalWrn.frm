VERSION 5.00
Begin VB.Form CalWrn 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "OPERATOR ALERT"
   ClientHeight    =   2616
   ClientLeft      =   2760
   ClientTop       =   3756
   ClientWidth     =   7932
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2616
   ScaleWidth      =   7932
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   3360
      TabIndex        =   0
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Verificare posizione testina"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   23.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   480
      TabIndex        =   2
      Top             =   960
      Width           =   6975
   End
   Begin VB.Label Label1 
      Caption         =   "Dati di calibrazione atipici"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   23.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   480
      TabIndex        =   1
      Top             =   240
      Width           =   6975
   End
End
Attribute VB_Name = "CalWrn"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub OKButton_Click()
        Unload Me
End Sub
