VERSION 5.00
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form S_MAG 
   Caption         =   "S_MAG Test Station"
   ClientHeight    =   9540
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11880
   DrawMode        =   11  'Not Xor Pen
   FillColor       =   &H80000001&
   ForeColor       =   &H000000FF&
   Icon            =   "S_MAG.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9540
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text2 
      Height          =   288
      Left            =   10140
      TabIndex        =   98
      Top             =   540
      Width           =   1512
   End
   Begin VB.TextBox Text1 
      Height          =   288
      Left            =   10140
      TabIndex        =   96
      Top             =   240
      Width           =   1512
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10320
      TabIndex        =   93
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   9480
      TabIndex        =   92
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   91
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7800
      TabIndex        =   90
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   89
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6120
      TabIndex        =   88
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   87
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   86
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   85
      Top             =   8640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox txt_DCoeff_max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   84
      Top             =   8640
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.TextBox txt_DCoeff_min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   83
      Top             =   8640
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.CommandButton Cmd_Verify 
      Caption         =   "Verify"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9000
      Style           =   1  'Graphical
      TabIndex        =   82
      Top             =   3120
      Width           =   975
   End
   Begin VB.TextBox txt_CoeffAvg_Min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   80
      Top             =   9120
      Width           =   855
   End
   Begin VB.TextBox txt_CoeffAvg_Max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   79
      Top             =   9120
      Width           =   855
   End
   Begin VB.TextBox txt_CoeffAvg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3600
      TabIndex        =   78
      Top             =   9120
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff_Min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   76
      Top             =   8280
      Width           =   855
   End
   Begin VB.TextBox txt_Coeff_Max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   75
      Top             =   8280
      Width           =   855
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   74
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   73
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   72
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6120
      TabIndex        =   71
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   70
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7800
      TabIndex        =   69
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   68
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   9480
      TabIndex        =   67
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Coeff 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10320
      TabIndex        =   66
      Top             =   8280
      Width           =   615
   End
   Begin VB.TextBox txt_Operator 
      Height          =   285
      Left            =   7500
      TabIndex        =   21
      Top             =   120
      Width           =   1095
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   1800
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
      BaudRate        =   19200
      StopBits        =   2
   End
   Begin MSComCtl2.Animation ani_Motion 
      Height          =   495
      Left            =   240
      TabIndex        =   63
      Top             =   3720
      Visible         =   0   'False
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   873
      _Version        =   393216
      FullWidth       =   33
      FullHeight      =   33
   End
   Begin MSComCtl2.Animation ani_PoliceLight 
      Height          =   495
      Left            =   240
      TabIndex        =   62
      Top             =   120
      Visible         =   0   'False
      Width           =   495
      _ExtentX        =   873
      _ExtentY        =   873
      _Version        =   393216
      FullWidth       =   33
      FullHeight      =   33
   End
   Begin VB.CheckBox PRN_EN 
      Caption         =   "Printer_Enable"
      Height          =   375
      Left            =   10080
      TabIndex        =   61
      Top             =   3120
      Width           =   1335
   End
   Begin VB.ComboBox Combo_ChartSel 
      Height          =   315
      ItemData        =   "S_MAG.frx":08CA
      Left            =   120
      List            =   "S_MAG.frx":08CC
      TabIndex        =   59
      Text            =   "Combo_ChartSel"
      Top             =   6120
      Width           =   1335
   End
   Begin MSChart20Lib.MSChart MSChart1 
      Height          =   2895
      Left            =   1320
      OleObjectBlob   =   "S_MAG.frx":08CE
      TabIndex        =   57
      Top             =   3600
      Width           =   10455
   End
   Begin VB.ListBox lst_msg 
      Height          =   645
      Left            =   120
      TabIndex        =   56
      Top             =   2640
      Width           =   8775
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10320
      TabIndex        =   55
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   9480
      TabIndex        =   54
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   53
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7800
      TabIndex        =   52
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   51
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6120
      TabIndex        =   50
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   49
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   48
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10320
      TabIndex        =   47
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   9480
      TabIndex        =   46
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   45
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7800
      TabIndex        =   44
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   43
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6120
      TabIndex        =   42
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   41
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   40
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   8
      Left            =   10320
      TabIndex        =   31
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   7
      Left            =   9480
      TabIndex        =   30
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   29
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7800
      TabIndex        =   28
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   27
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   6120
      TabIndex        =   26
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   25
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   24
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_PCode 
      Height          =   285
      Left            =   10260
      TabIndex        =   22
      Top             =   2640
      Visible         =   0   'False
      Width           =   1512
   End
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   20
      Left            =   840
      Top             =   120
   End
   Begin VB.Timer Timer1 
      Left            =   1320
      Top             =   120
   End
   Begin VB.TextBox txt_Noise_Peak 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   16
      Top             =   7200
      Width           =   615
   End
   Begin VB.TextBox txt_Signal 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   15
      Top             =   7920
      Width           =   615
   End
   Begin VB.TextBox txt_Noise_Avg 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   13
      Top             =   7560
      Width           =   615
   End
   Begin VB.TextBox txt_Signal_Max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   12
      Top             =   7920
      Width           =   855
   End
   Begin VB.TextBox txt_Signal_Min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   11
      Top             =   7920
      Width           =   855
   End
   Begin VB.TextBox txt_Noise_Avg_Max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   10
      Top             =   7560
      Width           =   855
   End
   Begin VB.TextBox txt_Noise_Avg_Min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   9
      Top             =   7560
      Width           =   855
   End
   Begin VB.TextBox txt_Noise_Peak_Max 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   2520
      TabIndex        =   8
      Top             =   7200
      Width           =   855
   End
   Begin VB.TextBox txt_Noise_Peak_Min 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0,000E+00"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1040
         SubFormatType   =   6
      EndProperty
      Enabled         =   0   'False
      Height          =   285
      Left            =   1560
      TabIndex        =   7
      Top             =   7200
      Width           =   855
   End
   Begin VB.TextBox txt_Result 
      Alignment       =   2  'Center
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FF00&
      Height          =   675
      Left            =   9360
      TabIndex        =   2
      Top             =   1800
      Width           =   1575
   End
   Begin VB.TextBox txt_SN 
      Height          =   285
      Left            =   10140
      TabIndex        =   3
      Top             =   960
      Width           =   1092
   End
   Begin VB.CommandButton Cmd_Start 
      Caption         =   "Start"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   9000
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   2640
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "Digital Board"
      Height          =   192
      Left            =   9060
      TabIndex        =   97
      Top             =   300
      Width           =   972
   End
   Begin VB.Label Lbl_lst_SN 
      Height          =   252
      Left            =   10080
      TabIndex        =   95
      Top             =   1320
      Width           =   1512
   End
   Begin VB.Label Label6 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CoeffDelta"
      Height          =   255
      Left            =   240
      TabIndex        =   94
      Top             =   8640
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Label7 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CalCoeffAvg"
      Height          =   255
      Left            =   240
      TabIndex        =   81
      Top             =   9120
      Width           =   975
   End
   Begin VB.Label Label5 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CalCoeff"
      Height          =   255
      Left            =   240
      TabIndex        =   77
      Top             =   8280
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Last S.N."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9120
      TabIndex        =   65
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label9 
      Caption         =   "Operator"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   252
      Left            =   6540
      TabIndex        =   64
      Top             =   120
      Width           =   732
   End
   Begin VB.Shape Shape2 
      Height          =   855
      Left            =   120
      Top             =   1680
      Width           =   8775
   End
   Begin VB.Label Label1 
      Caption         =   "Graph_Type"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   60
      Top             =   5880
      Width           =   1455
   End
   Begin VB.Label Test_Name 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   58
      Top             =   960
      Width           =   8535
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "9"
      Height          =   255
      Index           =   8
      Left            =   10440
      TabIndex        =   39
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "8"
      Height          =   255
      Index           =   7
      Left            =   9600
      TabIndex        =   38
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "7"
      Height          =   255
      Index           =   6
      Left            =   8760
      TabIndex        =   37
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "6"
      Height          =   255
      Index           =   5
      Left            =   7920
      TabIndex        =   36
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "5"
      Height          =   255
      Index           =   4
      Left            =   7080
      TabIndex        =   35
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "4"
      Height          =   255
      Index           =   3
      Left            =   6240
      TabIndex        =   34
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "3"
      Height          =   255
      Index           =   2
      Left            =   5400
      TabIndex        =   33
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "2"
      Height          =   255
      Index           =   1
      Left            =   4560
      TabIndex        =   32
      Top             =   6720
      Width           =   255
   End
   Begin VB.Label Label4 
      Caption         =   "Analog Board"
      Height          =   252
      Left            =   9060
      TabIndex        =   23
      Top             =   600
      Width           =   1032
   End
   Begin VB.Shape Shape10 
      Height          =   1935
      Left            =   1440
      Top             =   7080
      Width           =   9615
   End
   Begin VB.Label Label32 
      Alignment       =   2  'Center
      Caption         =   "CTS cashpro"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   615
      Left            =   2640
      TabIndex        =   20
      Top             =   120
      Width           =   3375
   End
   Begin VB.Line Line3 
      X1              =   9000
      X2              =   11760
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Shape Shape8 
      Height          =   855
      Left            =   120
      Top             =   840
      Width           =   8775
   End
   Begin VB.Label Operator_Msg 
      Alignment       =   2  'Center
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   240
      TabIndex        =   19
      Top             =   1800
      Width           =   8535
   End
   Begin VB.Shape Shape7 
      Height          =   2895
      Left            =   120
      Top             =   6600
      Width           =   11175
   End
   Begin VB.Label Label27 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Signal"
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   7920
      Width           =   975
   End
   Begin VB.Label Label26 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Noise_Peak"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   7200
      Width           =   975
   End
   Begin VB.Label Label25 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Noise_Avg"
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   7560
      Width           =   975
   End
   Begin VB.Shape v 
      Height          =   2412
      Left            =   9000
      Top             =   120
      Width           =   2772
   End
   Begin VB.Label Label19 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Max"
      Height          =   255
      Left            =   2640
      TabIndex        =   6
      Top             =   6720
      Width           =   735
   End
   Begin VB.Label Label18 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Min"
      Height          =   255
      Left            =   1560
      TabIndex        =   5
      Top             =   6720
      Width           =   735
   End
   Begin VB.Label Label3 
      Caption         =   "S.N."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9120
      TabIndex        =   4
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label_ch 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "1"
      Height          =   255
      Index           =   0
      Left            =   3720
      TabIndex        =   0
      Top             =   6720
      Width           =   255
   End
End
Attribute VB_Name = "S_MAG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const CHANNELS = 9             ' numero testine del sensore
Private Const MAX_CHANNELS = 18        ' numero testine massimo previsto dalla dll
    
Dim SptFileName As String              ' nome file spt con path e suffisso ".spt"
Dim FwFileName As String               ' nome file fw con path e suffisso ".hex"
Dim PrdFileName As String              ' nome file parametri del prodotto con path prefisso "T2-" e suffisso ".ini"
Dim IniFileName As String              ' nome file parametri del programma con path e suffisso ".ini"
Dim Cal_DB_FileName As String          ' nome file archivio coef.calibrazione delle testine campione
Dim RepFileName As String              ' nome file di report con path e suffisso ".txt"
Dim ReportPath As String               ' path da utilizzare per il file di report (opzionale)
Dim Last_Product As String             ' contiene l'ultimo codice prodotto collaudato
Dim Last_SN As String                  ' contiene l'ultimo SN collaudato
Dim last_verify As String              ' contiene la data dell'ultima verifica banco con testina campione
Dim last_operator As String            ' contiene l'identificativo dell'operatore addetto all'ultimo collaudo
Dim coeff_avg As Double                ' media coeff. di calibrazione
Dim CPASS, CFAIL, CTOT As Integer

'------------------------------- veriabili lette dal file di configurazione programma
Dim serial_port As Byte                ' definisce la porta seriale
Dim serial_motor_control As Byte       ' definisce la porta seriale per il controllo motore
Dim Cal_Acq_Timeout As Integer         ' definisce il timeout della fase di calibrazione (10s)
Dim Cal_Acq_Time As Integer            ' definisce il tempo di campionamento
Dim Cal_Ini_Delay As Integer           ' definisce il ritardo per consentire all'operatore il movimento (CCW)
Dim Power_On_Delay As Integer          ' definisce il ritardo dopo il comando POWERON
Dim dbg As Boolean                     ' definisce la modalita' debug o normale
Dim animotion As Boolean               ' letto da SMAG.ini definisce se attivare l'animazione movimento
Dim anilight As Boolean                ' letto da SMAG.ini definisce se attivare l'animazione police-light

Dim gcoeff_warn_ch_min As Single       ' limite generico min. per la generazione del messaggio di warning in calibrazione
Dim gcoeff_warn_ch_max As Single       ' limite generico max. per la generazione del messaggio di warning in calibrazione
Dim gcoeff_warn_avg_min As Single      ' limite generico min. per la generazione del messaggio di warning in calibrazione
Dim gcoeff_warn_avg_max As Single      ' limite generico max. per la generazione del messaggio di warning in calibrazione
Dim coeff_warn_ch_min As Single        ' limite specifico min. per la generazione del messaggio di warning in calibrazione
Dim coeff_warn_ch_max As Single        ' limite specifico max. per la generazione del messaggio di warning in calibrazione
Dim coeff_warn_avg_min As Single       ' limite specifico min. per la generazione del messaggio di warning in calibrazione
Dim coeff_warn_avg_max As Single       ' limite specifico max. per la generazione del messaggio di warning in calibrazione

Dim signal_target_min As Single        ' marker per grafico signal
Dim signal_target As Single            ' marker per grafico signal
Dim signal_target_max As Single        ' marker per grafico signal

Dim err_msg As String                  ' contiene un messaggio diagnostico

Dim Timer1_FLG As Boolean              ' Flag di appoggio per la gestione timer
Dim oc_lst_msg As Integer              ' variabile di appoggio per la gestione scroll del combo_list lst_msg
'------------------------------- veriabili lette dal file S_MAG_Cal_DB.ini
Dim verify_test_num As Integer
Dim coeff_ref_delta_max As Single
Dim cal_coeff(MAX_CHANNELS) As Single  ' vettore per coefficienti calibrazione delle testine campione
'------------------------------- veriabili lette dal file di configurazione prodotto
Dim fw As String                       ' nome file firmware
Dim spt As String                      ' nome fle spt
Dim hw_level As Byte                   ' Hardware level
Dim model_id As Byte                   ' ID Modello
Dim noise_peak_min As Byte             ' Limite minimo rumore di picco
Dim noise_peak_max As Byte             ' Limite massimo rumore di picco
Dim noise_avg_min As Byte              ' Limite minimo rumore medio
Dim noise_avg_max As Byte              ' Limite massimo rumore medio
Dim signal_min As Byte                 ' Limite minimo segnale
Dim signal_max As Byte                 ' Limite massimo segnale
Dim reference As Byte                  ' riferimento per calcolo coeffs

Dim target_gain As Single              ' Target_Gain per calcolo coeffs
Dim main_gain As Single                ' Main_Gain per calcolo coeffs
Dim coeffs_point As Byte               ' coeffs_point per calcolo coeffs
Dim serwarn As Integer
Dim SerialDgt As Long
Dim INIFILE$
Dim T2MAG$
Dim CNFG As String

'-------------------------------------------------------------------------------
Dim mgain As Double                    ' variabile di appoggio per il calcolo coeffs
Dim tgain As Double                    ' variabile di appoggio per il calcolo coeffs

Dim sn As Long                         ' contiene il serial number testina

Dim avg(MAX_CHANNELS) As Byte          ' vettore per rumore medio
Dim pk(MAX_CHANNELS) As Byte           ' vettore per rumore di picco
Dim acq(MAX_CHANNELS) As Byte          ' vettore per segnale
Dim acq_avg(MAX_CHANNELS) As Single    ' vettore per media segnale acquisito in fase di verifica
Dim coeffs(MAX_CHANNELS) As Single     ' vettore per coefficienti calcolati
Dim coeffs_buf(MAX_CHANNELS) As Single ' vettore per coefficienti calcolati da passare a MGP_WRITE
Dim coeffs_avg(MAX_CHANNELS) As Double ' vettore per media coefficienti calcolati
Dim coeff_delta(MAX_CHANNELS) As Single ' vettore per scostamento percentuale
                                        ' coefficienti di calibrazione nella verifica attrezzatura

Dim ofs(MAX_CHANNELS) As Integer       ' vettore per offset
Dim Gerr As Boolean                    ' Flag Pass/Fail Globale del collaudo
Dim SkipKeyCodeCheck As Boolean
Dim mgpchecklist_rc As Long             ' return code dll magpro nel noise_check
                                        ' il valore MGP_RES_OFFSET_ERROR implica il calcolo del vettore ofs()
                                        ' riportante l'offset delle testine .
                                        ' Tali valori di offset saranno esposti nel file report ed in stampa .

Dim mgpcalibrationacquire_rc As Long    ' return code dll magpro nella fase calibration acquire

'''sndPlaySound Constants
Const SND_ALIAS = &H10000
Const SND_ASYNC = &H1
Const SND_FILENAME = &H20000
Const SND_LOOP = &H8
Const SND_MEMORY = &H4
Const SND_NODEFAULT = &H2
Const SND_NOSTOP = &H10
Const SND_SYNC = &H0
Const SND_PURGE = &H40

Private Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" (ByVal lpszName As String, ByVal hModule As Long, ByVal dwFlags As Long) As Long
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Function GetPrivateProfileStringI(sAppName As String, sKeyName As String, skeydefault As String, iNumBytes As Integer, sFileName As String) As String
    Dim sSTR  As String * 255
    Dim lLong As Long
  
    lLong = GetPrivateProfileString(sAppName, sKeyName, skeydefault, sSTR, (iNumBytes), sFileName)
    GetPrivateProfileStringI = Left$(sSTR, lLong)
End Function

'------------------------------------------------------------------------------------------------------
' Cmd_Start_Click raggruppa tutta la sequenza di collaudo
Private Sub Cmd_Start_Click()
    Dim result As Long              ' memorizza il risultato delle chiamate alla dll magpro
    Dim mgp_write_result As Long    ' memorizza il risultato della chiamata alla MGPWriteCalibration
    Dim i As Byte
    Dim retry As Byte               ' variabile per gestione riciclo su errore
    Dim MGPWriteFlg As Boolean      ' Flag per condizionamento MGP_WriteCalibration
    Dim TMP_STR As String
    Dim MATX$
    
    Combo_ChartSel.ListIndex = 6    'SignalTarget   0   CalCoeffs
    
    Label6.Visible = False
    txt_DCoeff_min.Visible = False
    txt_DCoeff_max.Visible = False
    For i = 0 To CHANNELS - 1
        txt_DCoeff(i).Visible = False
    Next i
    
    Cmd_Start.Enabled = False
    Cmd_Verify.Enabled = False
    txt_Operator.Enabled = False
    txt_SN.Enabled = False
    Text1.Enabled = False
    Text2.Enabled = False
    
    Gerr = True
    mgpchecklist_rc = 0
    MGPWriteFlg = False
    txt_Result.Text = ""
    lst_msg.Clear
    
    Operator_Msg = ""
    '-- Inizializza Grafico a 0 e coefficienti calibrazione a zero
    For i = 0 To MAX_CHANNELS - 1
        pk(i) = 0
        avg(i) = 0
        acq(i) = 0
        coeffs(i) = 0
        ofs(i) = 0
    Next i
    
    coeff_avg = 0
    Call upg_chart
    result = upg_noise
    result = upg_acq
    Call upg_coeff
        
    sn = Val(txt_SN.Text)
    
    '-- Animazione 1 Police-Light
    If (anilight = True) Then
        ani_PoliceLight.Visible = True
        ani_PoliceLight.Play
    End If
    
    lst_msg.AddItem ("Start_Time: " & CStr(Now))
    lst_msg.AddItem ("Digital Board: " & Text1.Text)
    lst_msg.AddItem ("Analog Board: " & Text2.Text)
    lst_msg.AddItem ("SN: " & Str$(sn))
        
    If (MSComm1.PortOpen = False) Then
        MSComm1.PortOpen = True
    End If
    
    
    lst_msg.AddItem "POWERON command"
    MSComm1.Output = "POWERON" + Chr$(13)
    lst_msg.AddItem "WAIT Power_On_Delay=" + Str$(Power_On_Delay) + "mS"
    Call WTimer1_Expiration(Power_On_Delay)
    
    'Call WTimer1_Expiration(200)

    Do
        '---------------------------- Port Open ----------------------------------------------------
        Test_Name = "Connecting to DSP on Port" & Str$(serial_port)
        lst_msg.AddItem ("MGPOpen Port=" & serial_port)
        Call WTimer1_Expiration(100)
        result = MGPOpen(serial_port)
        If (result <> 0) Then
            Err_Diag (result)
            Exit Do
        Else
            Call Result_Pass
        End If
        
        '---------------------------- DSP Programming ----------------------------------------------
        Test_Name = "DSP Programming with " + fw
        For retry = 1 To 3 Step 1
            lst_msg.AddItem "DSP Programming Cycle" & Str$(retry)
            Call WTimer1_Expiration(100)
            result = MGPProgram(FwFileName, model_id, hw_level)
            If result = 0 Then
                Exit For
            Else
                Err_Diag (result)
            End If
        Next retry
        
        If (result <> 0) Then
            Exit Do
        Else
            Call Result_Pass
        End If
                
        '---------------------------- Loading SPT --------------------------------------------------
        Test_Name = "Loading SPT with " + spt
        For retry = 1 To 3 Step 1
            lst_msg.AddItem "Loading SPT Cycle" & Str$(retry)
            Call WTimer1_Expiration(100)
            result = MGPLoadSPT(SptFileName)
            If result = 0 Then
                Exit For
            Else
                Err_Diag (result)
            End If
        Next retry
        
        If (result <> 0) Then
            Exit Do
        Else
            Call Result_Pass
        End If
        
        '---------------------------- Setting Serial Number ----------------------------------------
        Test_Name = "Setting Serial Number =" + Str$(sn)
        For retry = 1 To 3 Step 1
            lst_msg.AddItem "Setting Serial Number Cycle" & Str$(retry)
            Call WTimer1_Expiration(100)
            result = MGPSetSerial(sn)
            If result = 0 Then
                Exit For
            Else
                Err_Diag (result)
            End If
        Next retry
        
        If (result <> 0) Then
            Exit Do
        Else
            Call Result_Pass
        End If
                
                
                
        '---------------------------- SCRITTURA MATRICOLA PIASTRA DIGITALE ----------------------------------------
        Test_Name = "Setting Digital Board Code"
        MATX$ = Mid$(Text1.Text, 12, 5)
        SerialDgt = dahexadec(MATX$)
        For retry = 1 To 3 Step 1
            lst_msg.AddItem "Setting Digital Board Code " + Mid$(Text1.Text, 12, 5) + "  " & Str$(retry)
            Call WTimer1_Expiration(100)
            result = MGPSetSerialDigital(SerialDgt)
            If result = 0 Then
                Exit For
            Else
                Err_Diag (result)
            End If
        Next retry
        
        If (result <> 0) Then
            Exit Do
        Else
            Call Result_Pass
        End If
                
        '---------------------------- Noise Check --------------------------------------------------
        Test_Name = "Noise Check..."
        MGPWriteFlg = True                  ' abilito la scrittura dati calibrazione
        For retry = 1 To 3 Step 1
        
            lst_msg.AddItem "Noise Check Cycle" & Str$(retry)
            Call WTimer1_Expiration(100)
            result = MGPChecklist(avg(0), pk(0))
            
            ' in assenza di errori del return_code dalla dll analizzo i vettori con i limiti
            If result = 0 Then result = upg_noise
            
            If result = 0 Then
                Exit For
            Else
                Err_Diag (result)
            End If
            
        Next retry
        
        mgpchecklist_rc = result
        
        If (result <> 0) Then
            ' verifico se l'errore e' sull'offset
            If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
                For i = 0 To CHANNELS - 1
                    ofs(i) = pk(i) + (avg(i) * 256)
                Next i
            End If
            Exit Do
        Else
            Call Result_Pass
        End If
        
        Call upg_chart
        
        '-- Animazione 2 Movimento
        If (animotion = True) Then
            ani_Motion.Visible = True
            ani_Motion.Play
        End If
        
        For retry = 1 To 3 Step 1
                
            '------------------------ Posizionamento iniziale Full CCW -----------------------------

            If retry > 1 Then
                Operator_Msg = "MOVE BACK SENSOR (CCW)"
                Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
                lst_msg.AddItem "CCW SENSOR Moving"
                lst_msg.AddItem "WAIT Cal_Ini_Delay=" + Str$(Cal_Ini_Delay) + "mS"
                Call WTimer1_Expiration(Cal_Ini_Delay)
            End If
            
            Test_Name = "Calibration Init..."
            Call WTimer1_Expiration(100)
                      
            lst_msg.AddItem "MGPCalibrationInit Cycle" & Str$(retry)
            result = MGPCalibrationInit(Cal_Acq_Time, False)
            If result = 0 Then
                Call Result_Pass
            Else
                Err_Diag (result)
            End If
        
            If (MSComm1.PortOpen = False) Then
                MSComm1.PortOpen = True
            End If
            lst_msg.AddItem "START command"
            MSComm1.Output = "START" + Chr$(13)
        
            '------------------------ Acquisizione dati calibrazione -------------------------------
            If (result = 0) Then
            
                Test_Name = "Calibration Acquire..."
                Operator_Msg = "MOVE SENSOR (CW)"
                lst_msg.AddItem "CW SENSOR Moving"
                Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
                
                lst_msg.AddItem "MGPCalibrationAcquire Cycle" & Str$(retry)
                Call WTimer1_Expiration(100)
                result = MGPCalibrationAcquire(acq(0), Cal_Acq_Timeout)
                If result = 0 Then result = upg_acq()
            
                If result = 0 Then
                    Exit For
                Else
                    Err_Diag (result)
                End If
            End If
            
        Next retry
               
        mgpcalibrationacquire_rc = result
        Call upg_chart
        
        If (result <> 0) Then
            Exit Do
        Else
            Call Result_Pass
        End If
        
        
        'Operator_Msg = "MOVE BACK SENSOR (CCW)"
        
        '-- Animazione 2
        If (animotion = True) Then
            ani_Motion.Stop
            ani_Motion.Visible = False
        End If
        
        '---------------------------- Calcolo coefficienti calibrazione ----------------------------
        tgain = target_gain
        tgain = tgain * (255 - reference)
        coeff_avg = 0
        For i = 0 To CHANNELS - 1
            mgain = main_gain
            mgain = mgain * (255 - acq(i))
            coeffs(i) = tgain / mgain
            coeff_avg = coeff_avg + coeffs(i)
        Next i
        coeff_avg = coeff_avg / CHANNELS
        
'        lst_msg.AddItem "Evaluated Calibration Data :"
'        For i = 0 To CHANNELS - 1
'            'lst_msg.AddItem "H" + Mid$(Str$(i + 1), 2) + "=" + Format(coeffs(i), "#0.00")
'            lst_msg.AddItem "H" + Mid$(Str$(i + 1), 2) + "=" + Str$(coeffs(i))
'        Next i
        
'        lst_msg.AddItem "Evaluated Calibration Data Average =" + Str$(coeff_avg)
        If (upg_coeff() <> 0) Then
            CalWrn.Show vbModal, Me
            'MsgBox " Dati di Calibrazione atipici" & Chr$(13) & Chr$(10) & " Verificare posizione testina", , "Operator Warning"
        End If
        Exit Do
     
    Loop
    
    If (animotion = True) Then
        If ani_Motion.Visible = True Then
            ani_Motion.Stop
            ani_Motion.Visible = False
        End If
    End If
    
    If result <> 0 Then Call Result_Fail ' aggiorno il fail eventuale , dell'ultima operazione
    '---- Su Errore Foto in MGPCalibrationAcquire attendo il completamento dell'operazione
    If result = &H54 Then
        lst_msg.AddItem "Attesa fine campionamento"
        lst_msg.AddItem "WAIT Cal_Acq_Time=" + Str$(Cal_Acq_Time) + "mS"
        Call WTimer1_Expiration(Cal_Acq_Time)
    End If
    '-------------------------------- Scrittura calibrazione in eeprom -----------------------------
    '-------------------------------- Eseguita anche in condizione di Fail successivi al caricamento SN
    mgp_write_result = 0
    If (MGPWriteFlg = True) Then
    
        Test_Name = "Write Calibration..."
        For retry = 1 To 3 Step 1
            For i = 0 To CHANNELS - 1
                coeffs_buf(i) = coeffs(i)
            Next i
            lst_msg.AddItem "MGPWriteCalibration Cycle" & Str$(retry)
            mgp_write_result = MGPWriteCalibration(True, coeffs_buf(0), coeffs_point)
            If mgp_write_result = 0 Then
                Exit For
            Else
                Err_Diag (mgp_write_result)
            End If
        Next retry
        
        Call upg_chart
        If (mgp_write_result <> 0) Then
            Call Result_Fail
        Else
            Call Result_Pass
        End If
    End If
    
    
    Test_Name = ""
    
    Call MGPClose
    lst_msg.AddItem ("MGPClose")
    lst_msg.AddItem ("End_Time: " & CStr(Time))
                         
    result = result Or mgp_write_result
    
    If result <> 0 Then
        
        Gerr = True
        txt_Result.ForeColor = RGB(&HFF, &H0, &H0)
        txt_Result.Text = "FAIL"
        CFAIL = CFAIL + 1
        Call upg_report("CAL")
        
        On Error GoTo printer_err
        
        If PRN_EN.Value = 1 Then
            Printer.Print "Serial Number:" + Str$(sn)
            Printer.Print
            Printer.Print " Date:" + CStr(Date) + " Time:" + CStr(Time)
            Printer.Print " Failing Test: " + Test_Name.Caption
            Printer.Print " Test Error: " & err_msg
            
            
            ' verifico se l'errore e' sull'offset
            If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
            
                Printer.Print " Offset:";
                For i = 0 To CHANNELS - 1
                    Printer.Print Str$(ofs(i)) & " ";
                Next i
                Printer.Print
           
            Else
           
                Printer.Print " Noise_Peak:";
                For i = 0 To CHANNELS - 1
                    Printer.Print Str$(pk(i)) & " ";
                Next i
                Printer.Print
                Printer.Print " Noise_Average:";
                For i = 0 To CHANNELS - 1
                    Printer.Print Str$(avg(i)) & " ";
                Next i
                Printer.Print
            
            End If
            
            Printer.Print " Signal:";
            For i = 0 To CHANNELS - 1
                Printer.Print Str$(acq(i)) & " ";
            Next i
            Printer.Print
            Printer.EndDoc
        End If
        
printer_err:
        
        On Error GoTo 0
        
    Else

        Gerr = False
        txt_Result.ForeColor = RGB(&H0, &H7F, &H0)
        txt_Result.Text = "PASS"
        CPASS = CPASS + 1
        Call upg_report("CAL")
    
    End If
                 
    If (MSComm1.PortOpen = False) Then
        MSComm1.PortOpen = True
    End If
    lst_msg.AddItem "POWEROFF command"
    MSComm1.Output = "POWEROFF" + Chr$(13)
    
    '-- Animazione 1
    If (anilight = True) Then
        ani_PoliceLight.Stop
        ani_PoliceLight.Visible = False
    End If
    
    Operator_Msg = "Power_Off,Remove Board"
    Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
    Call WTimer1_Expiration(2000)
    txt_Operator.Enabled = True
    
    Call WritePrivateProfileString("STATISTICA", "PASS_COUNTER", Str$(CPASS), IniFileName)
    Call WritePrivateProfileString("STATISTICA", "FAIL_COUNTER", Str$(CFAIL), IniFileName)
    Call WritePrivateProfileString("SOFTWARE", "LAST_PRODUCT", Text1.Text, IniFileName)
    
    last_operator = UCase(txt_Operator.Text)
    Call WritePrivateProfileString("SOFTWARE", "last_operator", last_operator, IniFileName)
    
    'txt_lst_SN.Text = txt_SN.Text
    TMP_STR = "00000000" + Mid$(Str$(sn), 2)
    TMP_STR = Right$(TMP_STR, 8)
    Lbl_lst_SN.Caption = TMP_STR
    Call WritePrivateProfileString("SOFTWARE", "Last_SN", TMP_STR, IniFileName)

    txt_SN.Text = ""
    txt_SN.Enabled = True
    txt_PCode.Enabled = False

    Call WTimer1_Expiration(200)
    txt_SN.SetFocus
    
    If (lst_msg.ListCount > 0) Then
        lst_msg.ListIndex = lst_msg.ListCount - 1
        lst_msg.ListIndex = -1
    End If
    
    'Timer2.Enabled = True
                                               
    TMP_STR = Format(Date, "dd-mm-yyyy")
    If (last_verify <> TMP_STR) Then
        SChkWrn.Show vbModal, Me
        'MsgBox " Verificare l'attrezzatura" & Chr$(13) & Chr$(10) & " con una testina campione", , "Operator alert"
    End If
                        
End Sub


'------------------------------------------------------------------------------------------------------

Private Sub Cmd_Verify_Click()

'------------------------------------------------------------------------------------------------------
    Dim result As Long              ' memorizza il risultato delle chiamate alla dll magpro
    Dim mgp_write_result As Long    ' memorizza il risultato della chiamata alla MGPWriteCalibration
    Dim i, acq_cycle As Byte
    Dim retry As Byte               ' variabile per gestione riciclo su errore
    Dim MGPWriteFlg As Boolean      ' Flag per condizionamento MGP_WriteCalibration
    Dim TMP_STR As String
    
    result = 1
    
    If (Load_Cal_DB(txt_SN.Text) = True) Then
        
        Combo_ChartSel.ListIndex = 5
        
        Label6.Visible = True
        txt_DCoeff_min.Visible = True
        txt_DCoeff_max.Visible = True
        For i = 0 To CHANNELS - 1
            txt_DCoeff(i).Visible = True
            txt_DCoeff(i).ForeColor = RGB(&HFF, &H0, &H0)
            txt_DCoeff(i).Text = "0"
        Next i
        
        Cmd_Start.Enabled = False
        Cmd_Verify.Enabled = False
        txt_Operator.Enabled = False
        txt_SN.Enabled = False
        txt_PCode.Enabled = False
        
        Gerr = True
        mgpchecklist_rc = 0
        MGPWriteFlg = False
        txt_Result.Text = ""
        lst_msg.Clear
        
        Operator_Msg = ""
        '-- Inizializza Grafico a 0 e coefficienti calibrazione a zero
        For i = 0 To MAX_CHANNELS - 1
            pk(i) = 0
            avg(i) = 0
            acq(i) = 0
            acq_avg(i) = 0
            coeffs(i) = 0
            ofs(i) = 0
            coeff_delta(i) = 0
        Next i
        
        coeff_avg = 0
        Call upg_chart
        result = upg_noise
        result = upg_acq
        Call upg_coeff
        'Call upg_coeff_delta
            
        sn = Val(txt_SN.Text)
            
        '-- Animazione 1 Police-Light
        If (anilight = True) Then
            ani_PoliceLight.Visible = True
            ani_PoliceLight.Play
        End If
        
        lst_msg.AddItem ("Start_Time: " & CStr(Time))
        lst_msg.AddItem ("PCode: " & txt_PCode.Text)
        lst_msg.AddItem ("SN: " & Str$(sn))
            
        If (MSComm1.PortOpen = False) Then
            MSComm1.PortOpen = True
        End If
        lst_msg.AddItem "POWERON command"
        MSComm1.Output = "POWERON" + Chr$(13)
        lst_msg.AddItem "WAIT Power_On_Delay=" + Str$(Power_On_Delay) + "mS"
        Call WTimer1_Expiration(Power_On_Delay)
        
        'Call WTimer1_Expiration(200)
    
        Do
            '---------------------------- Port Open ----------------------------------------------------
            Test_Name = "Connecting to DSP on Port" & Str$(serial_port)
            lst_msg.AddItem ("MGPOpen Port=" & serial_port)
            lst_msg.AddItem "WAIT 100mS"
            Call WTimer1_Expiration(100)
            lst_msg.AddItem "MGPOpen(serial_port=" & Mid$(Str$(serial_port), 2) & ")"
            result = MGPOpen(serial_port)
            lst_msg.AddItem "MGPOpen ReturnCode=" & Hex$(result) & "H"
            If (result <> 0) Then
                Err_Diag (result)
                Exit Do
            Else
                Call Result_Pass
            End If
            
            '---------------------------- Noise Check --------------------------------------------------
            Test_Name = "Noise Check..."
            MGPWriteFlg = True                  ' abilito la scrittura dati calibrazione
            For retry = 1 To 3 Step 1
            
                lst_msg.AddItem "Noise Check Cycle" & Str$(retry)
                lst_msg.AddItem "WAIT 100mS"
                Call WTimer1_Expiration(100)
                lst_msg.AddItem "MGPChecklist(avg,pk)"
                result = MGPChecklist(avg(0), pk(0))
                lst_msg.AddItem "MGPChecklist ReturnCode=" & Hex$(result) & "H"
                
                ' in assenza di errori del return_code dalla dll analizzo i vettori con i limiti
                If result = 0 Then result = upg_noise
                
                If result = 0 Then
                    Exit For
                Else
                    Err_Diag (result)
                End If
                
            Next retry
            
            mgpchecklist_rc = result
            
            If (result <> 0) Then
                ' verifico se l'errore e' sull'offset
                If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
                    For i = 0 To CHANNELS - 1
                        ofs(i) = pk(i) + (avg(i) * 256)
                    Next i
                End If
                Exit Do
            Else
                Call Result_Pass
            End If
            
            Call upg_chart
            
            '-- Animazione 2 Movimento
            If (animotion = True) Then
                ani_Motion.Visible = True
                ani_Motion.Play
            End If
            
            'For retry = 1 To 1 Step 1
            
            For i = 0 To MAX_CHANNELS - 1
                acq_avg(i) = 0
                coeffs_avg(i) = 0
            Next i
            tgain = target_gain
            tgain = tgain * (255 - reference)
            coeff_avg = 0
            
            For acq_cycle = 1 To verify_test_num
                    
                '------------------------ Posizionamento iniziale Full CCW -----------------------------
    
                If acq_cycle > 1 Then
                    Operator_Msg = "MOVE BACK SENSOR (CCW)"
                    Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
                    lst_msg.AddItem "CCW SENSOR Moving"
                    lst_msg.AddItem "WAIT Cal_Ini_Delay=" + Str$(Cal_Ini_Delay) + "mS"
                    Call WTimer1_Expiration(Cal_Ini_Delay)
                End If
                
                Test_Name = "Calibration Init..."
                Call WTimer1_Expiration(100)
                          
                lst_msg.AddItem "MGPCalibrationInit Cycle" & Str$(acq_cycle) ' & " Retry=" & Str$(retry)
                lst_msg.AddItem "MGPCalibrationInit(Cal_Acq_Time=" & Str$(Cal_Acq_Time) & ",False)"
                result = MGPCalibrationInit(Cal_Acq_Time, False)
                lst_msg.AddItem "MGPCalibrationInit ReturnCode=" & Hex$(result) & "H"
                If result = 0 Then
                    Call Result_Pass
                Else
                    Err_Diag (result)
                    Exit For
                End If
            
                If (MSComm1.PortOpen = False) Then
                    MSComm1.PortOpen = True
                End If
                
                lst_msg.AddItem "START command"
                MSComm1.Output = "START" + Chr$(13)
            
                '------------------------ Acquisizione dati calibrazione -------------------------------
                If (result = 0) Then
                
                    Test_Name = "Calibration Acquire..."
                    Operator_Msg = "MOVE SENSOR (CW)"
                    lst_msg.AddItem "CW SENSOR Moving"
                    Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
                                    
                    'For acq_cycle = 1 To verify_test_num
                        
                    lst_msg.AddItem "MGPCalibrationAcquire Cycle=" & Str$(acq_cycle) ' & " Retry=" & Str$(retry)
                    Call WTimer1_Expiration(100)
                    lst_msg.AddItem "MGPCalibrationAcquire(lpData as Byte,Cal_Acq_Timeout=" & Str$(Cal_Acq_Timeout) & ")"
                    result = result Or MGPCalibrationAcquire(acq(0), Cal_Acq_Timeout)
                    lst_msg.AddItem "MGPCalibrationAcquire ReturnCode=" & Hex$(result) & "H"
                    If result = 0 Then
                        For i = 0 To CHANNELS - 1
                            acq_avg(i) = acq_avg(i) + acq(i)
                            mgain = main_gain
                            mgain = mgain * (255 - acq(i))
                            coeffs(i) = tgain / mgain
                            coeffs_avg(i) = coeffs_avg(i) + coeffs(i) 'computo media
                        Next i
                    Else
                        Exit For
                            'result = upg_acq()
                    End If
                Else
                    Exit For
                End If
                    
            Next acq_cycle
                    
            If result = 0 Then
                For i = 0 To CHANNELS - 1
                    acq_avg(i) = acq_avg(i) / verify_test_num
                    acq(i) = acq_avg(i)                     ' riporto la media nel vettore acq
                                                            ' per i report e i grafici
                    coeffs_avg(i) = coeffs_avg(i) / verify_test_num
                    coeffs(i) = coeffs_avg(i)               ' riporto la media nel vettore coeffs
                                                            ' per i report e i grafici
                    coeff_avg = coeff_avg + coeffs_avg(i)   ' calcolo comunque il coeff
                                                            ' medio dei canali
                Next i
                coeff_avg = coeff_avg / CHANNELS
                Call upg_acq    ' update without error store
                'Exit For
            Else
                Err_Diag (result)
            End If
                
            'Next retry
                   
            mgpcalibrationacquire_rc = result
            Call upg_chart
            
            If (result <> 0) Then
                Exit Do
            Else
                Call Result_Pass
            End If
            
            
            'Operator_Msg = "MOVE BACK SENSOR (CCW)"
            
            '-- Animazione 2
            If (animotion = True) Then
                ani_Motion.Stop
                ani_Motion.Visible = False
            End If
            
            '---------------------------- Visualizzo  coefficienti calibrazione ----------------------------
            
            lst_msg.AddItem "Evaluated Delta Calibration Data :"
            For i = 0 To CHANNELS - 1
                coeff_delta(i) = 100 * (1 - coeffs(i) / cal_coeff(i))
                lst_msg.AddItem "H" + Mid$(Str$(i + 1), 2) + "=" + Str$(coeffs(i))
                coeff_delta(i) = Abs(coeff_delta(i))
            Next i
            
            Call upg_coeff                          ' update without error store
            result = result Or upg_coeff_delta()    ' update coeff_delta with error store
            
            Exit Do
         
        Loop
        
        If (animotion = True) Then
            If ani_Motion.Visible = True Then
        
                ani_Motion.Stop
                ani_Motion.Visible = False
        
            End If
        End If
        
        Call upg_chart
        If result <> 0 Then Call Result_Fail ' aggiorno il fail eventuale , dell'ultima operazione
        '---- Su Errore Foto in MGPCalibrationAcquire attendo il completamento dell'operazione
        If result = &H54 Then
                lst_msg.AddItem "Attesa fine campionamento"
                lst_msg.AddItem "WAIT Cal_Acq_Time=" + Str$(Cal_Acq_Time) + "mS"
                Call WTimer1_Expiration(Cal_Acq_Time)
        End If
            
    End If
    
        lst_msg.AddItem "WAIT Cal_Ini_Delay=" + Str$(Cal_Ini_Delay) + "mS"
        Call WTimer1_Expiration(Cal_Ini_Delay)
        
        Test_Name = ""
        
        Call MGPClose
        lst_msg.AddItem ("MGPClose")
        lst_msg.AddItem ("End_Time: " & CStr(Time))
                             
        
        If result <> 0 Then
            
            Gerr = True
            txt_Result.ForeColor = RGB(&HFF, &H0, &H0)
            txt_Result.Text = "FAIL"
            CFAIL = CFAIL + 1
            
            On Error GoTo printer_err
            
            If PRN_EN.Value = 1 Then
                Printer.Print "Serial Number:" + Str$(sn)
                Printer.Print
                Printer.Print " Date:" + CStr(Date) + " Time:" + CStr(Time)
                Printer.Print " Failing Test: " + Test_Name.Caption
                Printer.Print " Test Error: " & err_msg
                
                
                ' verifico se l'errore e' sull'offset
                If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
                
                    Printer.Print " Offset:";
                    For i = 0 To CHANNELS - 1
                        Printer.Print Str$(ofs(i)) & " ";
                    Next i
                    Printer.Print
               
                Else
               
                    Printer.Print " Noise_Peak:";
                    For i = 0 To CHANNELS - 1
                        Printer.Print Str$(pk(i)) & " ";
                    Next i
                    Printer.Print
                    Printer.Print " Noise_Average:";
                    For i = 0 To CHANNELS - 1
                        Printer.Print Str$(avg(i)) & " ";
                    Next i
                    Printer.Print
                
                End If
                
                Printer.Print " Signal:";
                For i = 0 To CHANNELS - 1
                    Printer.Print Str$(acq(i)) & " ";
                Next i
                Printer.Print
                Printer.EndDoc
            End If
            
printer_err:
            
            On Error GoTo 0
            
            Else
    
            Gerr = False
            txt_Result.ForeColor = RGB(&H0, &H7F, &H0)
            txt_Result.Text = "PASS"
            CPASS = CPASS + 1
            last_verify = Format(Date, "dd-mm-yyyy")
            Call WritePrivateProfileString("SOFTWARE", "last_verify", last_verify, IniFileName)
    
        End If
                     
        Call upg_report("VERIFY")
            
        If (MSComm1.PortOpen = False) Then
            MSComm1.PortOpen = True
        End If
        lst_msg.AddItem "POWEROFF command"
        MSComm1.Output = "POWEROFF" + Chr$(13)
        
        '-- Animazione 1
        If (anilight = True) Then
            ani_PoliceLight.Stop
            ani_PoliceLight.Visible = False
        End If
        
        Operator_Msg = "Power_Off,Remove Board"
        Call PlaySound("Ding.wav", 0, SND_ASYNC Or SND_FILENAME)
        Call WTimer1_Expiration(2000)
        txt_Operator.Enabled = True
        
        Call WritePrivateProfileString("STATISTICA", "PASS_COUNTER", Str$(CPASS), IniFileName)
        Call WritePrivateProfileString("STATISTICA", "FAIL_COUNTER", Str$(CFAIL), IniFileName)
        Call WritePrivateProfileString("SOFTWARE", "LAST_PRODUCT", txt_PCode.Text, IniFileName)
        
        last_operator = UCase(txt_Operator.Text)
        Call WritePrivateProfileString("SOFTWARE", "last_operator", last_operator, IniFileName)
        
        'txt_lst_SN.Text = txt_SN.Text
        TMP_STR = "00000000" + Mid$(Str$(sn), 2)
        TMP_STR = Right$(TMP_STR, 8)
        Lbl_lst_SN.Caption = TMP_STR
        Call WritePrivateProfileString("SOFTWARE", "Last_SN", TMP_STR, IniFileName)
    
        txt_SN.Text = ""
        txt_SN.Enabled = True
        txt_PCode.Enabled = True
    
        Call WTimer1_Expiration(200)
        txt_SN.SetFocus
        
        If (lst_msg.ListCount > 0) Then
            lst_msg.ListIndex = lst_msg.ListCount - 1
            lst_msg.ListIndex = -1
        End If
    
    'Timer2.Enabled = True
                                               
                        
End Sub

Private Sub Form_Load()

    Dim n As Integer
    Dim ini_str As String
    Dim fpath As String
    
    'Dim TMP_STR As String
    '    TMP_STR = Format(Date, "dd/mm/yyyy")
    
    SkipKeyCodeCheck = False
    
    '------ Inizializzo i parametri base del grafico
    With MSChart1.Legend
        .Location.Visible = True
        .Location.LocationType = VtChLocationTypeLeft
        .TextLayout.HorzAlignment = VtHorizontalAlignmentRight
        '.VtFont.VtColor.Set 0, 0, 0
        '.Backdrop.Fill.Style = VtFillStyleBrush
        '.Backdrop.Fill.Brush.Style = VtBrushStyleSolid
        '.Backdrop.Fill.Brush.FillColor.Set &H0, &H60, &H60
    End With


    With MSChart1.Title.VtFont
        .Style = VtFontStyleBold
        .Size = 10
        '.VtColor.Set 0, 0, 220
    End With
    MSChart1.Title.Location.Rect.Min.X = 4700
    MSChart1.Title.Location.Rect.Min.Y = 2500
    MSChart1.Title.Location.Rect.Max.X = 7700
    MSChart1.Title.Location.Rect.Max.Y = 2700
   ' MSChart1.Title.Location.LocationType = VtChLocationTypeCustom
    'MSChart1.Title.Location.LocationType = VtChLocationTypeTop
            
    
    '------ Inizializzo le voci del selettore tipo grafico
    Combo_ChartSel.AddItem ("CalCoeffs")
    Combo_ChartSel.AddItem ("Peak")
    Combo_ChartSel.AddItem ("Average")
    Combo_ChartSel.AddItem ("Signal")
    Combo_ChartSel.AddItem ("P.A.S.")
    Combo_ChartSel.AddItem ("CoeffDelta")
    Combo_ChartSel.AddItem ("SignalTarget")
    Combo_ChartSel.ListIndex = 0
    '------ Formatto il nome del file ini dell'applicazione
    If Right(App.Path, 1) = "\" Then
        fpath = App.Path
    Else
        fpath = App.Path & "\"
    End If
    IniFileName = fpath & "S_MAG.ini"
    Cal_DB_FileName = fpath & "S_MAG_Cal_DB.ini"
    
    '------ ReportPath definisce :
    '------ Se nullo il file report verr� generato nella cartella dell'applicazione
    '------ in caso contrario ReportPath e' assunto come directory di destinazione per il file report
    ReportPath = GetPrivateProfileStringI("SOFTWARE", "REPORT_PATH", "", 30, IniFileName)

    '------ controllo animazione motion
    ini_str = GetPrivateProfileStringI("SOFTWARE", "ANI_MOTION", "NO", 10, IniFileName)
    ini_str = UCase(ini_str)
    If (ini_str = "SI") Then
        animotion = True
    Else
        animotion = False
    End If
    '------ controllo animazione police_light
    ini_str = GetPrivateProfileStringI("SOFTWARE", "ANI_LIGHT", "NO", 10, IniFileName)
    ini_str = UCase(ini_str)
    If (ini_str = "SI") Then
        anilight = True
    Else
        anilight = False
    End If
    '------ DEBUG_MODE determina se visualizzare il list_box che traccia tutte le fasi del collaudo
    ini_str = GetPrivateProfileStringI("SOFTWARE", "DEBUG_MODE", "NO", 10, IniFileName)
    ini_str = UCase(ini_str)
    If (ini_str = "SI") Then
        dbg = True
    Else
        dbg = False
    End If
    '------ STAMPA abilita la stampa della diagnosi nel caso di piastra FAIL
    ini_str = GetPrivateProfileStringI("SOFTWARE", "STAMPA", "SI", 10, IniFileName)
    ini_str = UCase(ini_str)
    If (ini_str = "SI") Then
        PRN_EN.Value = 1
        txt_Operator.SetFocus
    Else
        PRN_EN.Value = 0
    End If
    '------ Cal_Acq_Time stabilisce il tempo di acquisizione durante la calibrazione
    Cal_Acq_Time = GetPrivateProfileInt("SOFTWARE", "Cal_Acq_Time", 2000, IniFileName)
    '------ Cal_Acq_Timeout stabilisce il timeout della fase acquisizione segnale in calibrazione
    Cal_Acq_Timeout = GetPrivateProfileInt("SOFTWARE", "Cal_Acq_Timeout", 10000, IniFileName)
    '------ Cal_Ini_Delay stabilisce il ritardo generato per consentire all'operatore il movimento CCW
    '------ prima della verifica con MGPCalibrationInit
    Cal_Ini_Delay = GetPrivateProfileInt("SOFTWARE", "Cal_Ini_Delay", 2000, IniFileName)
    '------ Power_On_Delay stabilisce il ritardo da applicare dopo il comando POWERON
    Power_On_Delay = GetPrivateProfileInt("SOFTWARE", "Power_On_Delay", 1500, IniFileName)
    '------ SERIAL_PORT seleziona la porta seriale da utilizzare
    ini_str = GetPrivateProfileStringI("HARDWARE", "COM_PORT", "1", 10, IniFileName)
    serial_port = Val(ini_str)
    '------ SERIAL_PORT seleziona la porta seriale da utilizzare per controllo motore
    ini_str = GetPrivateProfileStringI("HARDWARE", "COM_MOTOR_CONTROL", "2", 10, IniFileName)
    serial_motor_control = Val(ini_str)
    MSComm1.CommPort = serial_motor_control
    '------ LAST_PRODUCT memorizza l'ultimo codice prodotto collaudato
    Last_Product = GetPrivateProfileStringI("SOFTWARE", "LAST_PRODUCT", "", 30, IniFileName)
    Last_Product = UCase(Last_Product)
    txt_PCode = Last_Product
    '------ LAST_SN memorizza l'ultimo serial number collaudato
    Last_SN = GetPrivateProfileStringI("SOFTWARE", "LAST_SN", "", 30, IniFileName)
    Lbl_lst_SN.Caption = Last_SN
    '------ LAST_VERIFY memorizza la data dell'ultima verifica del banco con una testina campione
    last_verify = GetPrivateProfileStringI("SOFTWARE", "last_verify", "01/01/1970", 12, IniFileName)
    '------ LAST_OPERATOR memorizza l'ID dell'ultimo operatore
    last_operator = GetPrivateProfileStringI("SOFTWARE", "last_operator", "", 30, IniFileName)
    last_operator = UCase(last_operator)
    '------ coeff_warn_ch_min memorizza il limite minimo per la generazione del messaggio warning in calibrazione
    ini_str = GetPrivateProfileStringI("SOFTWARE", "coeff_warn_ch_min", "1.50", 10, IniFileName)
    gcoeff_warn_ch_min = Val(ini_str)
    '------ coeff_warn_ch_max memorizza il limite massimo per la generazione del messaggio warning in calibrazione
    ini_str = GetPrivateProfileStringI("SOFTWARE", "coeff_warn_ch_max", "1.85", 10, IniFileName)
    gcoeff_warn_ch_max = Val(ini_str)
    '------ coeff_warn_avg_min memorizza il limite minimo per la generazione del messaggio warning in calibrazione
    ini_str = GetPrivateProfileStringI("SOFTWARE", "coeff_warn_avg_min", "1.55", 10, IniFileName)
    gcoeff_warn_avg_min = Val(ini_str)
    '------ coeff_warn_avg_max memorizza il limite massimo per la generazione del messaggio warning in calibrazione
    ini_str = GetPrivateProfileStringI("SOFTWARE", "coeff_warn_avg_max", "1.77", 10, IniFileName)
    gcoeff_warn_avg_max = Val(ini_str)
    
        
    '------ signal_target_min memorizza il marker minimo per il grafico signal
    ini_str = GetPrivateProfileStringI("SOFTWARE", "signal_target_min", "90", 10, IniFileName)
    signal_target_min = Val(ini_str)
    '------ signal_target_max memorizza il marker massimo per il grafico signal
    ini_str = GetPrivateProfileStringI("SOFTWARE", "signal_target_max", "110", 10, IniFileName)
    signal_target_max = Val(ini_str)
    '------ signal_target memorizza il marker target per il grafico signal
    ini_str = GetPrivateProfileStringI("SOFTWARE", "signal_target", "100", 10, IniFileName)
    signal_target = Val(ini_str)
        
    '------ Contatori Test Pass e Fail
    CPASS = GetPrivateProfileInt("STATISTICA", "PASS_COUNTER", 0, IniFileName)
    CFAIL = GetPrivateProfileInt("STATISTICA", "FAIL_COUNTER", 0, IniFileName)

    '------ Inizializzo le animazioni
    If anilight = True Then
        ani_PoliceLight.Open (fpath + "police-light.avi")
    End If
    If animotion = True Then
        ani_Motion.Open (fpath + "cogs2.avi")
    End If
    
    If dbg = True Then
        lst_msg.Visible = True
    Else
        lst_msg.Visible = False
    End If

    Timer2.Enabled = True
    Me.Visible = True
    txt_Operator.SetFocus
End Sub


Private Sub Form_Unload(Cancel As Integer)

    If anilight = True Then ani_PoliceLight.Close
    If animotion = True Then ani_Motion.Close

End Sub

Private Sub Text1_GotFocus()
    Operator_Msg = "Inserire cod. piastra DIGITALE"
    Test_Name = ""

End Sub

Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And Text1.Text <> "" Then
        If text1_Err_Check = True Then
            Text1.SetFocus
        Else
            Operator_Msg = "Inserire cod. piastra ANALOGICA"
            Text2.SetFocus
        End If
    End If
End Sub

Private Sub Text1_Validate(Cancel As Boolean)
    If text1_Err_Check = True Then
        Cancel = True
    End If

End Sub

Private Sub Text2_GotFocus()
    Operator_Msg = "Inserire cod. piastra ANALOGICA"
    Test_Name = ""

End Sub

Private Sub Text2_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 And Text2.Text <> "" Then
        If text2_Err_Check = True Then
            Text2.SetFocus
            KeyCode = 0
        Else
            Operator_Msg = "INSERIRE S.N."
            txt_SN.SetFocus
        End If
    End If

End Sub

Private Sub Text2_Validate(Cancel As Boolean)
    If text2_Err_Check = True Then
        Cancel = True
    End If


End Sub

Private Sub Timer1_Timer()

    Timer1_FLG = True

End Sub


'---- Sub utilizzata per generare ritardi tramite DoEvents
'---- Il tempo di attesa e' anche utilizzato per generare lo scroll del list_box (lst_msg)
Private Sub WTimer1_Expiration(exp_time As Integer)
            
    Timer1.Enabled = False
    Timer1.Interval = exp_time
    Timer1_FLG = False
    Timer1.Enabled = True
            
    While (Timer1_FLG = False)
            
        If (lst_msg.ListCount > 0) And (lst_msg.ListCount <> oc_lst_msg) Then
            oc_lst_msg = lst_msg.ListCount
            lst_msg.ListIndex = lst_msg.ListCount - 1
            lst_msg.ListIndex = -1
        End If
        DoEvents
                
    Wend

End Sub

Private Sub Timer2_Timer()

    txt_Operator.SetFocus
    Timer2.Enabled = False
    
End Sub

Private Sub txt_Operator_GotFocus()
    'txt_Operator.BackColor = &H80000005
    Operator_Msg = "INSERIRE il Codice Operatore"
    Test_Name = ""
    
End Sub
Private Sub txt_Operator_LostFocus()

    'txt_Operator.BackColor = &H808000
    'Operator_Msg = ""

End Sub
'---- Su pressione CR nel campo Operator verifica la presenza errori
'---- su OK passa il controllo al SerialNumber
'---- su FAIL mantiene il focus su Operator
Private Sub txt_Operator_KeyUp(KeyCode As Integer, Shift As Integer)

    Dim cur_date As String
    
    If KeyCode = 13 Then

        If Operator_Err_Check = True Then
            txt_Operator.SetFocus
            Else

            cur_date = Format(Date, "dd-mm-yyyy")
            If (UCase(txt_Operator.Text) <> last_operator) Or (last_verify <> cur_date) Then
                SChkWrn.Show vbModal, Me
                'MsgBox " Verificare l'attrezzatura con una testina campione", , "Operator alert"
                last_operator = UCase(txt_Operator.Text)
                Call WritePrivateProfileString("SOFTWARE", "last_operator", last_operator, IniFileName)
            End If
    
            Text1.Enabled = True
            Text1.SetFocus
        End If

    End If

End Sub
Private Sub txt_Operator_Validate(KeepFocus As Boolean)

    If Operator_Err_Check = True Then
        KeepFocus = True
    End If

End Sub

'---- Funzione di verifica presenza errori nel campo Operatore
'---- Ritorna True se presenti: (caratteri non ammessi) o (lunghezza 2< > 8 caratteri) o (LoadProductParameter error)
Function Operator_Err_Check() As Boolean

Dim n, C As Integer
Dim err As Boolean
    
    For n = 1 To Len(txt_Operator.Text)
        C = Asc(Mid$(txt_Operator.Text, n))
        If (C >= &H30 And C <= &H39) Or (C >= &H40 And C <= &H5A) Or (C >= &H61 And C <= &H7A) Or C = &H5F Or C = &H2D Then
            err = False
            Else
            err = True
            Exit For
        End If
    Next n
    
    If err = True Then
        
        MsgBox " Carattere non ammesso", , "Errore Codice Operatore"

        Else
    
        If Len(txt_Operator.Text) > 10 Or Len(txt_Operator.Text) < 2 Then
            err = True
            MsgBox " Lunghezza maggiore di 10 o minore di 2 caratteri ", , "Errore Codice Operatore"
            'Else
            'err = Load_Product_Parameter
        End If
    
    End If
        
Operator_Err_Check = err

End Function

Private Sub txt_PCode_GotFocus()
    
    'txt_PCode.BackColor = &H80000005
    Operator_Msg = "INSERIRE Codice Prodotto"
    Test_Name = ""
    
End Sub

Private Sub txt_PCode_LostFocus()
    
    'txt_PCode.BackColor = &H808000

End Sub
Private Sub txt_PCode_KeyUp(KeyCode As Integer, Shift As Integer)

    If KeyCode = &HD Then
        If PCode_Err_Check = True Then
            txt_PCode.SetFocus
        Else
            Operator_Msg = "INSERIRE SERIAL NUMBER TESTINA"
            txt_SN.SetFocus
        End If
    End If

End Sub

Private Sub txt_PCode_Validate(KeepFocus As Boolean)

    If PCode_Err_Check = True Then
        KeepFocus = True
    End If

End Sub

Private Sub txt_SN_GotFocus()
    
    'txt_SN.BackColor = &H80000005
    Cmd_Start.Enabled = False
    Cmd_Verify.Enabled = False
    Operator_Msg = "INSERIRE Serial Number"
    Test_Name = ""
    
End Sub
Private Sub txt_SN_LostFocus()

    'txt_SN.BackColor = &H808000

End Sub
'Private Sub txt_SN_Validate(KeepFocus As Boolean)
'
'   If SN_Err_Check = True Then
'        KeepFocus = True
'    End If
'
'End Sub

'---- Su pressione CR nel campo Serial Number verifica la presenza errori
'---- su OK passa il controllo al CommandButton START
'---- su FAIL mantiene il focus su Serial Number
Private Sub txt_SN_KeyUp(KeyCode As Integer, Shift As Integer)
 
 
'    If KeyCode <> &HD And SkipKeyCodeCheck = True Then
'        SkipKeyCodeCheck = False
'    End If
    
    If KeyCode = &HD And SkipKeyCodeCheck = False Then

        If SN_Err_Check = True Then
            'MsgBox "", , "Errore Serial Number"
            txt_SN.SetFocus
        Else
            Operator_Msg = "Rewind,Put Board,Power_On,Start"
            Cmd_Start.Enabled = True
            Cmd_Verify.Enabled = True
            Cmd_Start.SetFocus
            DoEvents
        End If
        
    Else
        SkipKeyCodeCheck = False
        
    End If
    
End Sub

'---- Funzione per la verifica presenza file
Function ExistFile(sSpec As String) As Boolean

    On Error Resume Next
    Call FileLen(sSpec)
    ExistFile = (err = 0)
    
End Function

'---- Funzione per il caricamento parametri relativi al prodotto
'---- costruisce il nome file PrdFileName dal nome prodotto con prefisso "T2-" e suffisso ".ini"
'---- ritorna False se il file ini del prodotto esiste e se i file FW e SPT citati esistono
'----         True in caso contrario
Private Function Load_Product_Parameter(APAT$) As Boolean
    Dim prdname As String
    Dim err As Boolean

    err = True

'    prdname = "T2-" + txt_PCode.Text + ".ini"
'
'
    If Right(App.Path, 1) = "\" Then
        PrdFileName = App.Path & APAT$
    Else
        PrdFileName = App.Path & "\" & APAT$
    End If

    If ExistFile(PrdFileName) Then

        fw = GetPrivateProfileStringI("FW", "fw", "", 10, PrdFileName)
        spt = GetPrivateProfileStringI("FW", "spt", "", 10, PrdFileName)
    
        fw = fw + ".hex"
        spt = spt + ".spt"
        If Right(App.Path, 1) = "\" Then
            FwFileName = App.Path & fw
            SptFileName = App.Path & spt
        Else
            FwFileName = App.Path & "\" & fw
            SptFileName = App.Path & "\" & spt
        End If
    
        '''''' GIA' DA T2-MAG.INI   hw_level = GetPrivateProfileInt("ID", "hw_level", 0, PrdFileName)
        model_id = GetPrivateProfileInt("ID", "model_id", 0, PrdFileName)

        noise_peak_min = GetPrivateProfileInt("Param", "noise_peak_min", 0, PrdFileName)
        noise_peak_max = GetPrivateProfileInt("Param", "noise_peak_max", 255, PrdFileName)
        noise_avg_min = GetPrivateProfileInt("Param", "noise_avg_min", 0, PrdFileName)
        noise_avg_max = GetPrivateProfileInt("Param", "noise_avg_max", 255, PrdFileName)
        signal_min = GetPrivateProfileInt("Param", "signal_min", 0, PrdFileName)
        signal_max = GetPrivateProfileInt("Param", "signal_max", 0, PrdFileName)
        reference = GetPrivateProfileInt("Param", "reference", 0, PrdFileName)

        target_gain = Val(GetPrivateProfileStringI("Param", "target_gain", "0", 50, PrdFileName))
        main_gain = Val(GetPrivateProfileStringI("Param", "main_gain", "0", 50, PrdFileName))
        coeffs_point = GetPrivateProfileInt("Param", "coeffs_point", 0, PrdFileName)
        
        serwarn = GetPrivateProfileInt("SOFTWARE", "serial_warn_delta_max", 100, IniFileName)
        
        coeff_warn_ch_min = Val(GetPrivateProfileStringI(Right$("000" + Trim$(Str$(hw_level)), 3), "coeff_warn_ch_min", "0", 10, IniFileName))
        coeff_warn_ch_max = Val(GetPrivateProfileStringI(Right$("000" + Trim$(Str$(hw_level)), 3), "coeff_warn_ch_max", "0", 10, IniFileName))
        coeff_warn_avg_min = Val(GetPrivateProfileStringI(Right$("000" + Trim$(Str$(hw_level)), 3), "coeff_warn_avg_min", "0", 10, IniFileName))
        coeff_warn_avg_max = Val(GetPrivateProfileStringI(Right$("000" + Trim$(Str$(hw_level)), 3), "coeff_warn_avg_max", "0", 10, IniFileName))

        If (coeff_warn_ch_min = 0) Then coeff_warn_ch_min = gcoeff_warn_ch_min
        If (coeff_warn_ch_max = 0) Then coeff_warn_ch_max = gcoeff_warn_ch_max
        If (coeff_warn_avg_min = 0) Then coeff_warn_avg_min = gcoeff_warn_avg_min
        If (coeff_warn_avg_max = 0) Then coeff_warn_avg_max = gcoeff_warn_avg_max

        txt_Noise_Peak_Min.Text = noise_peak_min
'        txt_Noise_Peak_Max.Text = noise_peak_max
        txt_Noise_Avg_Min.Text = noise_avg_min
'        txt_Noise_Avg_Max.Text = noise_avg_max
        txt_Signal_Min.Text = signal_min
        txt_Signal_Max.Text = signal_max
    
        txt_Coeff_Min.Text = Mid$(Str$(coeff_warn_ch_min), 2)
        txt_Coeff_Max.Text = Mid$(Str$(coeff_warn_ch_max), 2)
    
        txt_CoeffAvg_Min.Text = Mid$(Str$(coeff_warn_avg_min), 2)
        txt_CoeffAvg_Max.Text = Mid$(Str$(coeff_warn_avg_max), 2)
    
    
        If Not ExistFile(FwFileName) Then
            MsgBox " Fw_File : " + FwFileName + " Not Found", , "Errore Prodotto"
        Else
            If Not ExistFile(SptFileName) Then
                MsgBox "Spt_File : " + SptFileName + " Not Found", , "Errore Prodotto"
            Else
                Call Ini_Chart
                err = False
            End If
        End If
    
        Else
    
        MsgBox "Product_File : " + PrdFileName + " Not Found", , "Errore Prodotto"
    
    End If
    
Load_Product_Parameter = err

End Function
'---- Funzione di verifica presenza errori nel campo SerialNumber
'---- Ritorna True se presenti: (caratteri non numerici) o (lunghezza > 8 caratteri) o (valore < 1)
Function SN_Err_Check() As Boolean

    Dim n, C As Integer
    Dim err As Boolean
    
    err = False
    For n = 1 To Len(txt_SN.Text)
        C = Asc(Mid$(txt_SN, n))
        If (C < &H30 Or C > &H39) Then
            err = True
            Exit For
        End If
    Next n
    
    If err = True Then
        'txt_Result.SetFocus
        
        MsgBox "Carattere non ammesso", , "Errore Serial Number"
        SkipKeyCodeCheck = True
    Else
    
        If (Len(txt_SN.Text) > 8) Or (Val("0" + txt_SN.Text) < 1) Then
            'txt_Result.SetFocus
            MsgBox "", , "Errore Serial Number"
            SkipKeyCodeCheck = True
            err = True
        End If
        
    End If
    
    If Abs(Val(txt_SN.Text) - Val(Lbl_lst_SN.Caption)) > serwarn Then
      txt_SN.ForeColor = &HFF&
      DoEvents
    Else
      txt_SN.ForeColor = &H80000008
      DoEvents
    End If
    
SN_Err_Check = err
    
End Function

'---- Funzione di verifica presenza errori nel campo CodiceProdotto
'---- Ritorna True se presenti: (caratteri non ammessi) o (lunghezza > 8 caratteri) o (LoadProductParameter error)
Function PCode_Err_Check() As Boolean
    
    Dim n, C As Integer
    Dim err As Boolean
    
    For n = 1 To Len(txt_PCode.Text)
        C = Asc(Mid$(txt_PCode, n))
        If (C >= &H30 And C <= &H39) Or (C >= &H40 And C <= &H5A) Or (C >= &H61 And C <= &H7A) Or C = &H5F Or C = &H2D Then
            err = False
        Else
            err = True
            Exit For
        End If
    Next n
    
    If err = True Then
        
        MsgBox " Carattere non ammesso", , "Errore Prodotto"

        Else
    
        If Len(txt_PCode.Text) > 8 Then
            err = True
            MsgBox " Lunghezza > di 8 caratteri", , "Errore Prodotto"
        Else
'''''''''''''''''''''''''''''''''''''            err = Load_Product_Parameter
        End If
    
    End If
        
PCode_Err_Check = err

End Function

'---- Err_Diag ricava un messaggio diagnostico da un codice errore fornito come parametro
'---- Visualizza il messaggio diagnostico sul ListBox lst_msg
'---- Memorizza il messaggio diagnostico nella variabile globale err_msg
Sub Err_Diag(ByVal result As Long)


    Select Case result
    
    
        Case &H10
                err_msg = "Porta seriale non disponibile"
        Case &H11
                err_msg = "Errore nella comunicazione seriale"
        Case &H20
                err_msg = "Connessione fallita con il DSP del sensore"
        Case &H21
                err_msg = "Inizializzazione DSP del sensore fallita"
        Case &H30
                err_msg = "Errore di lettura file Intel-Hex del firmware"
        Case &H31
                err_msg = "File Intel-Hex del firmware non valido"
        Case &H40
                err_msg = "Errore nella programmazione della flash"
        Case &H41
                err_msg = "Errore nell'inizializzazione della SPT"
        Case &H50
                err_msg = "Tensione di alimentazione testina magnetica fuori tolleranza"
        Case &H51
                err_msg = "Livelli di offset elettrico fuori tolleranza"
        Case &H52
                err_msg = "Livelli di rumore magnetico troppo elevati"
        Case &H53
                err_msg = "Il foto di sincronismo non � libero all'inizio della calibrazione"
        Case &H54
                err_msg = "il Foto di sincronismo non si � occupato entro il timeout"
        Case &H99
                err_msg = "Valori rilevati fuori limite"
        Case Else
                err_msg = "Codice Errore sconosciuto"
        
    End Select

    err_msg = "Err_Msg " & Hex$(result) & "H " & err_msg
    lst_msg.AddItem err_msg
    'lst_msg.AddItem "Err_Msg :" & Hex$(result) & "=" & err_msg

End Sub

Sub Result_Pass()

    lst_msg.AddItem Test_Name & " Pass"

End Sub
Sub Result_Fail()

    lst_msg.AddItem Test_Name & " Fail"

End Sub

'---- Ini_Chart Inizializza il grafico in funzione della selezione tipo grafico (Combo_ChartSel)
Private Sub Ini_Chart()
    Dim n, smin, smax As Integer
    Dim sc_min, sc_max As Double
        
    MSChart1.Plot.Axis(VtChAxisIdX).AxisScale.Hide = True
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Auto = False
        
    Select Case Combo_ChartSel.ListIndex
    
        Case 0
            
            MSChart1.Title.Text = "CalCoeff"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 4
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "CalCoeff"
            MSChart1.Column = 4
            MSChart1.ColumnLabel = "CoeffAvg"
            MSChart1.Plot.SeriesCollection.Item(4).Pen.Style = VtPenStyleNative
            
            
            smin = coeff_warn_ch_min * 100
            smax = coeff_warn_ch_max * 100
            
            MSChart1.Column = 1
            MSChart1.ColumnLabel = "HighLim"
            MSChart1.Plot.SeriesCollection.Item(1).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = smax
            Next n
            MSChart1.Column = 3
            MSChart1.ColumnLabel = "LowLim"
            MSChart1.Plot.SeriesCollection.Item(3).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = smin
            Next n
            
            sc_min = smin
            sc_max = smax
            
            'sc_min = sc_min - 0.1 * sc_min
            sc_min = sc_min - (0.05 * sc_max)
            'sc_min = Int(sc_min / 10) * 10
            sc_min = Int(sc_min)
            If sc_min < 0 Then sc_min = 0
            
            sc_max = sc_max + (0.05 * sc_max)
            'sc_max = Int(sc_max / 10) * 10
            sc_max = Int(sc_max)
            'If sc_max > 255 Then sc_max = 255
            
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = sc_max
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = sc_min

        
        Case 1
            MSChart1.Title.Text = "Noise_Peak"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 3
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "Peak"
            Call Set_Scale(noise_peak_min, 255) ' noise_peak_max)
        
        Case 2
            MSChart1.Title.Text = "Noise_Average"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 3
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "Average"
            Call Set_Scale(noise_avg_min, 255) 'noise_avg_max)
    
        Case 3
            MSChart1.Title.Text = "Signal"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 3
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "Signal"
            Call Set_Scale(signal_min, signal_max)
            

        Case 4
            MSChart1.Title.Text = "NoisePK.,NoiseAVG.,Signal"
            MSChart1.chartType = VtChChartType2dBar
            MSChart1.ColumnCount = 3
            MSChart1.Column = 1
            MSChart1.ColumnLabel = "Peak"
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "Average"
            MSChart1.Column = 3
            MSChart1.ColumnLabel = "Signal"
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = 260
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = 0
    
        Case 5
        
            MSChart1.Title.Text = "Coeff_Delta"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 3
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "CoeffDelta"
            
            smin = 0
            smax = coeff_ref_delta_max * 100
            
            MSChart1.Column = 1
            MSChart1.ColumnLabel = "HighLim"
            MSChart1.Plot.SeriesCollection.Item(1).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = smax
            Next n
            MSChart1.Column = 3
            MSChart1.ColumnLabel = "LowLim"
            MSChart1.Plot.SeriesCollection.Item(3).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = smin
            Next n
            
            sc_min = smin
            sc_max = smax
            sc_min = sc_min - (0.1 * sc_max)
            sc_min = Int(sc_min)
            If sc_min < 0 Then sc_min = 0
            
            sc_max = sc_max + (0.1 * sc_max)
            sc_max = Int(sc_max)
            'If sc_max > 255 Then sc_max = 255
            
            
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = sc_max
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = sc_min

        Case 6
            
            MSChart1.Title.Text = "SignalTarget"
            MSChart1.chartType = VtChChartType2dLine
            MSChart1.ColumnCount = 4
            
            MSChart1.Column = 2
            MSChart1.ColumnLabel = "Signal"
                        
            MSChart1.Column = 1
            MSChart1.ColumnLabel = "TargetMax"
            MSChart1.Plot.SeriesCollection.Item(1).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = signal_target_max
            Next n
            MSChart1.Column = 3
            MSChart1.Plot.SeriesCollection.Item(3).Pen.Style = VtPenStyleDashed
            MSChart1.ColumnLabel = "TargetMin"
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = signal_target_min
            Next n
            
            MSChart1.Column = 4
            MSChart1.ColumnLabel = "Target"
            MSChart1.Plot.SeriesCollection.Item(4).Pen.Style = VtPenStyleDashed
            For n = 1 To CHANNELS
                MSChart1.Row = n
                MSChart1.Data = signal_target
            Next n
            
            sc_min = signal_target_min
            sc_max = signal_target_max
            
            sc_min = sc_min - (0.05 * sc_max)
            sc_min = Int(sc_min)
            If sc_min < 0 Then sc_min = 0
            
            sc_max = sc_max + (0.05 * sc_max)
            sc_max = Int(sc_max)
            If sc_max > 255 Then sc_max = 255
            
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = sc_max
            MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = sc_min


    End Select
            
End Sub

'---- Combo_ChartSel_Click su cambio tipo grafico richiama le due sub di inizializzazione e upgrade dati
Private Sub Combo_ChartSel_Click()
    Call Ini_Chart
    Call upg_chart
End Sub

'---- SetScale ricalcola la scala dell'asse Y e i cursori dei limiti minimo e massimo
'---- in funzione dei valori smin e smax passati come parametri
Sub Set_Scale(ByVal smin As Byte, ByVal smax As Byte)
    Dim sc_min, sc_max As Double
    Dim n As Integer
            
    MSChart1.Column = 1
    MSChart1.ColumnLabel = "HighLim"
    MSChart1.Plot.SeriesCollection.Item(1).Pen.Style = VtPenStyleDashed
    For n = 1 To CHANNELS
        MSChart1.Row = n
        MSChart1.Data = smax
    Next n
    MSChart1.Column = 3
    MSChart1.ColumnLabel = "LowLim"
    MSChart1.Plot.SeriesCollection.Item(3).Pen.Style = VtPenStyleDashed
    For n = 1 To CHANNELS
        MSChart1.Row = n
        MSChart1.Data = smin
    Next n
    
    sc_min = smin
    sc_max = smax
    'sc_max = sc_max + 0.1 * sc_max
    sc_max = sc_max + (0.1 * (smax - smin))
    sc_max = Int(sc_max)
    'sc_max = Int(sc_max / 10) * 10
    If sc_max > 255 Then sc_max = 255
    'sc_min = sc_min - 0.1 * sc_min
    sc_min = sc_min - (0.1 * (smax - smin))
    'sc_min = Int(sc_min / 10) * 10
    sc_min = Int(sc_min)
    If sc_min < 0 Then sc_min = 0
    
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Maximum = sc_max
    MSChart1.Plot.Axis(VtChAxisIdY).ValueScale.Minimum = sc_min

End Sub

'---- upg_chart aggiorna i dati del grafico in funzione del tipo grafico selezionato (Combo_ChartSel)
Sub upg_chart()

    Dim n, cs As Integer
        
    cs = Combo_ChartSel.ListIndex
    
    
    For n = 1 To CHANNELS
        
        Select Case cs
            Case 0
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = coeffs(n - 1) * 100 'quiqui
                MSChart1.Column = 4
                MSChart1.Row = n
                MSChart1.Data = coeff_avg * 100
            Case 1
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = pk(n - 1)
            Case 2
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = avg(n - 1)
            Case 3
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = acq(n - 1)
            Case 4
                MSChart1.Column = 1
                MSChart1.Row = n
                MSChart1.Data = pk(n - 1)
            
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = avg(n - 1)
            
                MSChart1.Column = 3
                MSChart1.Row = n
                MSChart1.Data = acq(n - 1)
            
            Case 5
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = coeff_delta(n - 1) * 100
        
            Case 6
                MSChart1.Column = 2
                MSChart1.Row = n
                MSChart1.Data = acq(n - 1)
        
        End Select
        
    Next n

End Sub

'---- upg_report costruisce la stringa RepFileName dal nome prodotto con suffisso ".txt"
'---- viene aggiunto il path che puo' essere ReportPath se citato nel SMAG.ini o App.path in caso contrario
'---- Il file RepFileName viene aperto in APPEND ed in esso viene scritto il SerialNumber, data e ora del collaudo
Sub upg_report(rpt_type As String)
    
'Dim progressivo As String
'Dim repstr As String
'Dim i As Integer
'
'    If (ReportPath <> "") Then
'
'        RepFileName = ReportPath & txt_PCode.Text & ".txt"
'
'        Else
'
'        If Right(App.Path, 1) = "\" Then
'            RepFileName = App.Path & txt_PCode.Text & ".txt"
'            Else
'            RepFileName = App.Path & "\" & txt_PCode.Text & ".txt"
'        End If
'
'    End If
'
'    progressivo = Mid$(Str$(sn), 2)
'    progressivo = "00000000" + progressivo
'    progressivo = Right$(progressivo, 8)
'
'    Open RepFileName For Append As #1
'
'    If Gerr = False Then
'
'        repstr = Mid$(Str$(CPASS), 2)
'        repstr = "000000" + repstr
'        repstr = Right$(repstr, 6)
'
'        Print #1, txt_Operator.Text & ";" & progressivo & ";" & Date$ & ";" & Time$ & ";" & repstr & ";PASS;"
'
'        Else
'
'        repstr = txt_Operator.Text & ";" & progressivo & ";" & Date$ & ";" & Time$ & ";FAIL;" & Test_Name.Caption & ";" & err_msg & ";"'
'
'        If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
'
'                repstr = repstr & ";Offset;"
'                For i = 0 To CHANNELS - 1
'                    repstr = repstr & Str$(ofs(i)) & ";"
'                Next i
'
'                Else
'
'                If (mgpchecklist_rc <> 0) Then
'
'                    repstr = repstr & ";Noise_pk;"
'                    For i = 0 To CHANNELS - 1
'                        repstr = repstr & Str$(pk(i)) & ";"
'                    Next i
'
'                    repstr = repstr & ";Noise_avg;"
'                    For i = 0 To CHANNELS - 1
'                        repstr = repstr & Str$(avg(i)) & ";"
'                    Next i
'
'                End If
'
'        End If
'
'        If (mgpcalibrationacquire_rc <> 0) Then
'            repstr = repstr & ";Signal;"
'            For i = 0 To CHANNELS - 1
'                repstr = repstr & Str$(acq(i)) & ";"
'            Next i
'        End If
'
'        Print #1, repstr
'
'    End If
'
'    Close #1
'
    
    
    
    
    
    
    
    
    Dim progressivo, msg As String
    Dim i As Integer
    Dim str_date As String
    Dim NOMFIL$
    Dim rifnom$
    Dim IDNUM$

    NOMFIL$ = CNFG
    If Right(App.Path, 1) = "\" Then
        rifnom$ = App.Path & NOMFIL$
    Else
        rifnom$ = App.Path & "\" & NOMFIL$
    End If

    str_date = Format(Date, "dd/mm/yyyy")
    NOMFIL$ = INIFILE$ + "-" + Trim$(Str$(Year(Now)))
    
    If (ReportPath <> "") Then
    
        RepFileName = ReportPath & NOMFIL$ & ".txt"
        
    Else
    
        If Right(App.Path, 1) = "\" Then
            RepFileName = App.Path & NOMFIL$ & ".txt"
        Else
            RepFileName = App.Path & "\" & NOMFIL$ & ".txt"
        End If
     
    End If

'    progressivo = txt_SN.Text 'Mid$(Str$(sn), 2)
    progressivo = Mid$(Str$(CPASS), 2)
    progressivo = "00000000" + progressivo
    progressivo = Right$(progressivo, 8)
    
    If ExistFile(RepFileName) Then
        Open RepFileName For Append As #1
    Else
        Open RepFileName For Append As #1
        Print #1, "Progressivo Operatore CodDigitale CodAnalogica SerialNumber Data Ora Esito TipoTest DiagMsg IDProdotto coeff_warn_avg_min coeff_warn_avg_max coeff_warn_avg ";
        Print #1, "coeff_warn_ch_min coeff_warn_ch_max ";
        For i = 1 To CHANNELS
            Print #1, "coeff" + Mid$(Str$(i), 2) & " ";
        Next i
        Print #1, "signal_min signal_max ";
        For i = 1 To CHANNELS
            Print #1, "signal" + Mid$(Str$(i), 2) & " ";
        Next i
        Print #1, "noise_avg_min ";
        For i = 1 To CHANNELS
            Print #1, "noise_avg" + Mid$(Str$(i), 2) & " ";
        Next i
        Print #1, "noise_peak_min ";
        For i = 1 To CHANNELS
            Print #1, "noise_peak" + Mid$(Str$(i), 2) & " ";
        Next i
        For i = 1 To CHANNELS
            Print #1, "offset" + Mid$(Str$(i), 2) & " ";
        Next i
        Print #1, ""
    End If
    
    Call rpt_add_int(progressivo)
    Call rpt_add_str(txt_Operator.Text)
    Call rpt_add_str(Mid$(Text1.Text, 2, 5) + "-" + Mid$(Text1.Text, 7, 2))
    Call rpt_add_str(Mid$(Text2.Text, 2, 5) + "-" + Mid$(Text2.Text, 7, 2))
    Call rpt_add_str(txt_SN.Text)
    Call rpt_add_str(str_date)
    Call rpt_add_str(Time$)
    If Gerr = False Then
        Call rpt_add_str("OK")
        msg = ""
    Else
        Call rpt_add_str("FAIL")
        msg = err_msg
'        msg = err_msg(0)
'        If err_msg_idx > 1 Then
'            msg = msg & " | " & err_msg(1)
'        End If
    End If
    
    Call rpt_add_str(rpt_type)
'    Call rpt_add_str("VERIFY")
    
    Call rpt_add_str(msg)
    
    IDNUM$ = GetPrivateProfileStringI("ID", "idParamMag_T2", "", 10, rifnom$)
''''''''''''''''''''''''''''''''''''''''''''''    Call rpt_add_str(txt_PCode.Text)
    Call rpt_add_str(IDNUM$)
    
    Call rpt_add_fp(coeff_warn_avg_min)
    Call rpt_add_fp(coeff_warn_avg_max)
    Call rpt_add_fp(coeff_avg)
    
    Call rpt_add_fp(coeff_warn_ch_min)
    Call rpt_add_fp(coeff_warn_ch_max)
        
    For i = 0 To CHANNELS - 1
        Call rpt_add_fp(coeffs(i))
    Next i
    
    Call rpt_add_int(signal_min)
    Call rpt_add_int(signal_max)
        
    For i = 0 To CHANNELS - 1
        Call rpt_add_int(acq(i))
    Next i
    
    Call rpt_add_int(noise_avg_min)
        
    For i = 0 To CHANNELS - 1
        Call rpt_add_int(avg(i))
    Next i
    
    Call rpt_add_int(noise_peak_min)
        
    For i = 0 To CHANNELS - 1
        Call rpt_add_int(pk(i))
    Next i
    
    For i = 0 To CHANNELS - 1
        Call rpt_add_int(ofs(i))
    Next i
    
    Print #1, ""
    
    Close #1
    
      
End Sub


    
'�  i dati che devono essere registrati tassativamente sono i seguenti:
'o nome operatore
'o matricola testina
'o data (formato tassativo "DD/MM/YYYY")
'o ora (formato tassativo "hh:mm:ss")
'o esito OK/FAIL
'o tipo di test ("CAL" per calibrazione standard, "VERIFY" per verifica attrezzatura)
'o descrizione dell'errore come riportato a video e nel caso l'errore sia generato dalla DLL
'includere sempre la stringa o il codice numerico di ritorno (stringa vuota "" se OK)
'o ID del file parametrico utilizzato (sezione ID, chiave idParamMag_T2)
'o soglie di warning della media dei coefficienti (coeff_warn_avg_min/max)
'o media dei coefficienti (coeff_avg, 0 se non disponibile)
'o soglie di warning dei coefficienti per singolo canale (coeff_warn_ch_min/max)
'o array dei coefficienti calcolati (cal_coeff, 0 se non disponibile)
'o soglie di controllo del segnale (signal_min/max)
'o array dei segnali acquisiti (signal tutti a 0 se non disponibile)
'o soglie di controllo del rumore medio (noise_avg_min)
'o array del rumore medio (noise_avg, 0 se non disponibile)
'o soglie di controllo del rumore di picco (noise_peak_min)
'o array del rumore di picco (noise_peak, 0 se non disponibile)
'o array degli offset (0 se non disponibile
    
    
    

'            Printer.Print "Serial Number:" + Str$(sn)
'            Printer.Print
'            Printer.Print " Date:" + CStr(Date) + " Time:" + CStr(Time)
'            Printer.Print " Failing Test: " + Test_Name.Caption
'            Printer.Print " Test Error: " & err_msg
'
'
'            ' verifico se l'errore e' sull'offset
'            If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
'
'                Printer.Print " Offset:";
'                    For i = 0 To CHANNELS - 1
'                        Printer.Print Str$(ofs(i)) & " ";
'                    Next i
'                Printer.Print
'
'                Else
'
'                Printer.Print " Noise_Peak:";
'                    For i = 0 To CHANNELS - 1
'                        Printer.Print Str$(pk(i)) & " ";
'                    Next i
'                Printer.Print
'                Printer.Print " Noise_Average:";
'                    For i = 0 To CHANNELS - 1
'                        Printer.Print Str$(avg(i)) & " ";
'                    Next i
'                Printer.Print
'
'            End If
'
'            Printer.Print " Signal:";
'                For i = 0 To CHANNELS - 1
'                    Printer.Print Str$(acq(i)) & " ";
'                Next i
'            Printer.Print
'            Printer.EndDoc
'        End If
'
'            ' verifico se l'errore e' sull'offset
'            If (mgpchecklist_rc = MGP_RES_OFFSET_ERROR) Then
'

'    'progressivo = Mid$(Str$(sn), 2)
'    progressivo = Mid$(Str$(CPASS), 2)
'    progressivo = "00000000" + progressivo
'    progressivo = Right$(progressivo, 8)
'
'    Open RepFileName For Append As #1
'    If Gerr = False Then
'        Print #1, progressivo & ";" & Mystr & ";" & Time$ & ";PASS;"
'        Else
'        msg = progressivo & ";" & Mystr & ";" & Time$ & ";FAIL " & err_msg(0)
'        If err_msg_idx > 1 Then
'            msg = msg & " | " & err_msg(1)
'        End If
'        msg = msg & ";"
'        Print #1, msg
'    End If
'    Close #1
    
      
    
      
Sub rpt_add_int(ByVal par As Integer)

    If (par < 0) Then
        par = par * -1
        Print #1, "-0" & Mid$(Str$(par), 2) & " ";
        Else
        Print #1, Mid$(Str$(par), 2) & " ";
    End If
    '    Print #1, Format(par, "0# ");

End Sub
Sub rpt_add_fp(ByVal fp_par As Double)
    Dim i_par As Integer
    fp_par = fp_par * 1000
    i_par = fp_par
    If (i_par < 0) Then
        i_par = i_par * -1
        Print #1, "-0" & Mid$(Str$(i_par), 2) & " ";
    Else
        Print #1, Mid$(Str$(i_par), 2) & " ";
    End If
End Sub
Sub rpt_add_str(ByRef par As String)
        
        Print #1, Chr$(34) & par & Chr$(34) & " ";

End Sub

'---- upg_acq aggiorna i dati del segnale acquisito nella tabella a video
'---- il ForeColor sara' rosso per valori fuori limite e verde in caso contrario
'---- La funzione restituisce &h99 nel caso di almeno un valore fuori limite
Function upg_acq() As Long

    Dim i As Byte
    Dim result As Long
                
    result = 0
    For i = 0 To CHANNELS - 1
                
        If (acq(i) < signal_min Or acq(i) > signal_max) Then
            result = &H99
            txt_Signal(i).ForeColor = RGB(&HFF, &H0, &H0)
            Else
            txt_Signal(i).ForeColor = RGB(&H0, &H0, &HFF)
        End If
                                             
        txt_Signal(i).Text = Mid$(Str$(acq(i)), 2)
                
    Next i

    upg_acq = result

End Function

'---- upg_noise aggiorna i dati del rumore di picco e medio nella tabella a video
'---- il ForeColor sara' rosso per valori fuori limite e verde in caso contrario
'---- La funzione restituisce &h99 nel caso di almeno un valore fuori limite
Function upg_noise() As Long

    Dim i As Byte
    Dim result As Long

    result = 0
    For i = 0 To CHANNELS - 1
                
        If (pk(i) < noise_peak_min) Then
            result = &H99
            txt_Noise_Peak(i).ForeColor = RGB(&HFF, &H0, &H0)
            Else
            txt_Noise_Peak(i).ForeColor = RGB(&H0, &H0, &HFF)
        End If
                 
            If (avg(i) < noise_avg_min) Then
            result = &H99
            txt_Noise_Avg(i).ForeColor = RGB(&HFF, &H0, &H0)
            Else
            txt_Noise_Avg(i).ForeColor = RGB(&H0, &H0, &HFF)
        End If
                              
        txt_Noise_Avg(i).Text = Mid$(Str$(avg(i)), 2)
        txt_Noise_Peak(i).Text = Mid$(Str$(pk(i)), 2)
                
    Next i
    upg_noise = result

End Function

'---- upg_coeff aggiorna i dati dei coef. di calibrazione nella tabella a video
'---- il ForeColor sara' rosso per valori fuori limite e verde in caso contrario
'---- La funzione restituisce &h99 nel caso di almeno un valore fuori limite
Function upg_coeff() As Long

    Dim i As Byte
    Dim result As Long
    Dim cf_avg As Double
    Dim cf As Single
    
    result = 0
    For i = 0 To CHANNELS - 1
        cf = Round(coeffs(i), 2)
        If (cf < coeff_warn_ch_min) Or (cf > coeff_warn_ch_max) Then
            result = &H99
            txt_Coeff(i).ForeColor = RGB(&HFF, &H0, &H0)
        Else
            txt_Coeff(i).ForeColor = RGB(&H0, &H0, &HFF)
        End If
                                             
'        txt_Coeff(i).Text = Format(coeffs(i), "#0.00")
        txt_Coeff(i).Text = Mid$(Str$(cf), 2)
                
    Next i
    
    cf_avg = Round(coeff_avg, 2)
    If (cf_avg < coeff_warn_avg_min) Or (cf_avg > coeff_warn_avg_max) Then
        result = &H99
        txt_CoeffAvg.ForeColor = RGB(&HFF, &H0, &H0)
    Else
        txt_CoeffAvg.ForeColor = RGB(&H0, &H0, &HFF)
    End If
'    txt_CoeffAvg.Text = Format(coeff_avg, "#0.00")
    txt_CoeffAvg.Text = Mid$(Str$(cf_avg), 2)
    
    upg_coeff = result

End Function



'---- upg_coeff aggiorna i dati dei coef. di calibrazione nella tabella a video
'---- il ForeColor sara' rosso per valori fuori limite e verde in caso contrario
'---- La funzione restituisce &h99 nel caso di almeno un valore fuori limite
Function upg_coeff_delta() As Long

    Dim i As Byte
    Dim result As Long
    Dim cf As Single
            
    result = 0
    For i = 0 To CHANNELS - 1
                
        cf = Round(coeff_delta(i), 2)
        If (cf > coeff_ref_delta_max) Then
            result = &H99
            txt_DCoeff(i).ForeColor = RGB(&HFF, &H0, &H0)
        Else
            txt_DCoeff(i).ForeColor = RGB(&H0, &H0, &HFF)
        End If
                                             
        txt_DCoeff(i).Text = Mid$(Str$(cf), 2)
                
    Next i
    
    upg_coeff_delta = result

End Function
'---- Funzione per il caricamento parametri relativi al prodotto
'---- costruisce il nome file PrdFileName dal nome prodotto con prefisso "T2-" e suffisso ".ini"
'---- ritorna False se il file ini del prodotto esiste e se i file FW e SPT citati esistono
'----         True in caso contrario
Private Function Load_Cal_DB(sn_section As String) As Boolean
    Dim sts As Boolean
    Dim i As Integer
        sts = False
    sn_section = "00000000" + sn_section
    sn_section = Right$(sn_section, 8)
    If ExistFile(Cal_DB_FileName) Then
 
        verify_test_num = GetPrivateProfileInt("Common", "verify_test_num", 5, Cal_DB_FileName)
        coeff_ref_delta_max = Val(GetPrivateProfileStringI("Common", "coeff_ref_delta_max", "3.0", 10, Cal_DB_FileName))
    
        txt_DCoeff_min.Text = ""
        txt_DCoeff_max.Text = Mid$(Str$(coeff_ref_delta_max), 2)
        
        For i = 0 To CHANNELS - 1
            cal_coeff(i) = Val(GetPrivateProfileStringI(sn_section, "coeff_" + Mid$(Str$(i), 2), "999", 10, Cal_DB_FileName))
        Next i
        sts = True
        For i = 0 To CHANNELS - 1
            If cal_coeff(i) = 999 Then
                sts = False
            End If
        Next i
        
        If sts = False Then
            MsgBox "Missing Calibration_Data for SN " + sn_section, , "Error"
        End If
        
        Else
    
        MsgBox "Calibration_DataBase_File : " + Cal_DB_FileName + " Not Found", , "Errore"
      
    End If
    

Load_Cal_DB = sts

End Function








'Function MGPOpen(ByVal nComPort As Byte) As Long
'    MGPOpen = 0
'End Function
'Function MGPProgram(ByVal szFwFile As String, ByVal bModel As Byte, ByVal bHwLevel As Byte) As Long
'    MGPProgram = 0
'End Function
'Function MGPLoadSPT(ByVal szSPTFile As String) As Long
'    MGPLoadSPT = 0
'End Function
'Function MGPSetSerial(ByVal dwSerial As Long) As Long
'    MGPSetSerial = 0
'End Function
'Function MGPChecklist(ByRef lpNoiseAvg As Byte, ByRef lpNoisePeaks As Byte) As Long
'    avg(0) = 255
'    avg(1) = 255
'    avg(2) = 255
'    avg(3) = 255
'    avg(4) = 255
'    avg(5) = 253
'    avg(6) = 254
'    avg(7) = 255
'    avg(8) = 253
'    pk(0) = 255
'    pk(1) = 255
'    pk(2) = 255
'    pk(3) = 255
'    pk(4) = 248
'    pk(5) = 247
'    pk(6) = 255
'    pk(7) = 254
'    pk(8) = 247
'    MGPChecklist = 0
'End Function
'Function MGPCalibrationInit(ByVal wTime As Integer, ByVal bVerify As Long) As Long
'    MGPCalibrationInit = 0
'End Function
'Function MGPCalibrationAcquire(ByRef lpData As Byte, ByVal wTimeout As Integer) As Long
'    acq(0) = 92
'    acq(1) = 52
'    acq(2) = 51
'    acq(3) = 52
'    acq(4) = 51
'    acq(5) = 103
'    acq(6) = 100
'    acq(7) = 54
'    acq(8) = 57
'    MGPCalibrationAcquire = 0
'End Function
'Function MGPWriteCalibration(ByVal bResult As Long, ByRef lpCoeffs As Single, ByVal bPoint As Byte) As Long
'    MGPWriteCalibration = 0
'End Function


'---- Funzione di verifica presenza errori nel campo CodiceProdotto
'---- Ritorna True se presenti: (caratteri non ammessi) o (lunghezza > 8 caratteri) o (LoadProductParameter error)
Function text1_Err_Check() As Boolean

    Dim n, C As Integer
    Dim err As Boolean
  'CHECK CARATTERI
    err = CKBC(Text1.Text)
        
    text1_Err_Check = err

End Function

Public Function CKBC(BC$) As Boolean
    Dim C As Integer
    Dim C1$
    Dim RES
    
    CKBC = True
    If Len(BC$) <> 16 Then
        RES = MsgBox("ERRORE LUNGHEZZA CAMPO (DEVE ESSERE DI 16 CARATTERI)", vbCritical)
        Exit Function
    End If
    C1$ = Mid$(BC$, 1, 1)
    C = Asc(C1$)
    If (C >= &H40 And C <= &H5A) Or (C >= &H61 And C <= &H7A) Then
    Else
        RES = MsgBox("ERRORE PRIMO CARATTERE", vbCritical)
        Exit Function
    End If
    
    CKBC = False
End Function
Function text2_Err_Check() As Boolean

    Dim n, C As Integer
    Dim err As Boolean
    Dim CodeDgt, RevDgt, CodeAn, RevAn As String
    Dim SEZC$
    Dim RES As String

    If Right(App.Path, 1) = "\" Then
        T2MAG$ = App.Path + "T2-MAG.INI"
        Else
        T2MAG$ = App.Path + "\T2-MAG.INI"
    End If

  'CHECK CARATTERI
    err = CKBC(Text2.Text)
    If Not err Then
        'ricerca file con dati per la coppia
        CodeDgt = Mid$(Text1.Text, 2, 5)
        CodeAn = Mid$(Text2.Text, 2, 5)
        RevDgt = Mid$(Text1.Text, 7, 2)
        RevAn = Mid$(Text2.Text, 7, 2)
        SEZC$ = CodeDgt + "-" + RevDgt + "_" + CodeAn + "-" + RevAn
        CNFG = GetPrivateProfileStringI(SEZC$, "ini_file", "", 100, T2MAG$)
        INIFILE$ = CNFG
        If CNFG = "" Then
        'NON ESISTE LA CONFIGURAZIONE
            RES = MsgBox("CONFIGURAZIONE NON PRESENTE SU  T2_MAG.INI", vbCritical)
            Exit Function
        Else
        'LETTURA PARAMETRI
            hw_level = GetPrivateProfileStringI(SEZC$, "hw_level", "", 100, T2MAG$)
            CNFG = CNFG + ".INI"
            err = Load_Product_Parameter(CNFG)
        End If
    End If
    text2_Err_Check = err

End Function

Public Function dahexadec(NUMHEX$) As Long
    dahexadec = 13589
End Function
