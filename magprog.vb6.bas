Attribute VB_Name = "magprog"
'*************************************************************************************************************
'* $AppName     MAG Programmer DLL
'* $File        magprog.vb6.bas
'* $Version     1.0
'* $Author      Maurizio Nervo - CTS CashPro
'* $Copyright   Copyright (C) 2006
'*
'* Definizioni per l'interfacciamento alle funzioni della DLL magprog.dll per la programmazione di
'* firmware e SPT e per la calibrazione del sesnore magnetico Murata 9/18 canali.
'*
'* $Target      Microsoft Visual Basic 6.0
'*************************************************************************************************************

'*************************************************************************************************************
'* Costanti dei codici risultato delle funzioni
'*************************************************************************************************************
Public Const MGP_RES_OK As Long = &H0                           ' Nessun errore
Public Const MGP_RES_INVALID_COM As Long = &H10         ' Porta seriale non disponibile
Public Const MGP_RES_COM_ERROR As Long = &H11           ' Errore nella comunicazione seriale
Public Const MGP_RES_CONNECT_FAILED As Long = &H20      ' Connessione fallita con il DSP del sensore
Public Const MGP_RES_INIT_FAILED As Long = &H21         ' Inizializzazione DSP del sensore fallita
Public Const MGP_RES_FW_NOT_FOUND As Long = &H30        ' Errore di lettura file Intel-Hex del firmware
Public Const MGP_RES_INVALID_FW As Long = &H31          ' File Intel-Hex del firmware non valido
Public Const MGP_RES_PROGRAM_FAILED As Long = &H40      ' Errore nella programmazione della flash
Public Const MGP_RES_SPT_ERROR As Long = &H41           ' Errore nell'inizializzazione della SPT
Public Const MGP_RES_VCC_ERROR As Long = &H50           ' Tensione di alimentazione testina magnetica fuori tolleranza
Public Const MGP_RES_OFFSET_ERROR As Long = &H51        ' Livelli di offset elettrico fuori tolleranza
Public Const MGP_RES_HIGH_NOISE As Long = &H52          ' Livelli di rumore magnetico troppo elevati
Public Const MGP_RES_FOTO_ERROR As Long = &H53          ' Il foto di sincronismo non � libero all'inizio della calibrazione
Public Const MGP_RES_CAL_TIMEOUT As Long = &H54         ' il Foto di sincronismo non si � occupato entro il timeout

'*************************************************************************************************************
'* Module ID a seconda del lettore di destinazione
'*************************************************************************************************************/
Public Const MAG_ID_RS12 As Byte = 0                            ' Modello OEM_MG per lettore RS12
Public Const MAG_ID_RS15 As Byte = 1                            ' Modello READER_MG per lettore RS15
Public Const MAG_ID_LEF As Byte = 2                                     ' Futuro modello per lettore LEF

'*************************************************************************************************************
'* Funzioni esportate dalla DLL
'*************************************************************************************************************/
Public Declare Function MGPOpen Lib "magprog" (ByVal nComPort As Byte) As Long
Public Declare Sub MGPClose Lib "magprog" ()
Public Declare Function MGPProgram Lib "magprog" (ByVal szFwFile As String, ByVal bModel As Byte, ByVal bHwLevel As Byte) As Long
Public Declare Function MGPLoadSPT Lib "magprog" (ByVal szSPTFile As String) As Long
Public Declare Function MGPSetSerial Lib "magprog" (ByVal dwSerial As Long) As Long
Public Declare Function MGPChecklist Lib "magprog" (ByRef lpNoiseAvg As Byte, ByRef lpNoisePeaks As Byte) As Long
Public Declare Function MGPCalibrationInit Lib "magprog" (ByVal wTime As Integer, ByVal bVerify As Long) As Long
Public Declare Function MGPCalibrationAcquire Lib "magprog" (ByRef lpData As Byte, ByVal wTimeout As Integer) As Long
Public Declare Function MGPWriteCalibration Lib "magprog" (ByVal bResult As Long, ByRef lpCoeffs As Single, ByVal bPoint As Byte) As Long
Public Declare Function MGPSetSerialDigital Lib "magprog" (ByVal SerialDgt As Long)
